#!/usr/bin/perl
use strict;
use warnings;
use Error qw(:try);
use DB_File;
use Compress::LZF;

my $fname="$ARGV[0]";
my (%clones);
tie %clones, "DB_File", $fname, O_RDONLY, 0666, $DB_File::DB_HASH
	or die "cant open file  $fname\n";
sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}

my $offset = 0;
while (my ($codeC, $vs) = each %clones){
	my $code = safeDecomp ($codeC);
	my $lC = length($codeC);
	my $l = length($code);
	print "$offset\;$l\;$vs\;$lC\n";
	$offset++;
}
untie %clones;
