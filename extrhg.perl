use strict;
#  Author: Audris Mockus
#
#Extract each revision from hg log -v output
#

require "timelocal.pl";

my %paths = ();
my %parents = ();;
my ($rev, $branch, $tag, $cname,  $ctime, $comment) = ("","","","","","","","","","");
my ($getHeader, $getMessage, $getPaths) = (0, 0, 0);

sub output {
	foreach my $f (keys %paths){
		$comment =~ s/\r/ /g;
		$comment =~ s/\;/SEMICOLON/g;
		$comment =~ s/\n/__NEWLINE__/g;
		print "$rev\;$f\;$cname\;$ctime\;$branch\;$tag\;$comment\n";
	}
	my $parent = join '|', (keys %parents);
	%paths = ();
	%parents = ();
	($rev, $branch, $tag, $cname, $ctime, $comment) = ("","","","","","","","","","");
}


while(<STDIN>){
	chop ();	
	#catch end of last revision information
	if (!$getMessage && /^changeset:\s*(.*)$/){#the very first
		$rev = $1;
		$getHeader = 1;
	}
	if ($getHeader){
		$cname = $1 if (/^user:\s*(.*)$/);
		$ctime = $1 if (/^date:\s*(.*)$/);
		$tag = $1 if (/^tag:\s*(.*)$/);
		$branch = $1 if (/^branch:\s*(.*)$/);
		$parents{$1}++ if (/^parent:\s*(.*)$/);
		if (/^files:\s*(.*)$/){
			my $str = $1;
			my @fs = split(/\s+/, $str, -1);
			for my $f (@fs){
				$paths{$f}++;
			}
		}			
		if (/^\s*description:\s*$/){
			$getMessage = 1;
			$getHeader=0;
			next;
		}
		next;
	}
	if ($getMessage){
		if (/^changeset:\s*(.*)$/){
			my $newrev=$1;
			&output ();
			$getHeader=1;
			$getMessage = 0;
			$rev = $newrev;
			next;
		}else{
			$comment .= "$_ NEWLINE";
		}
	}
}

&output ();#the very last
