use TokyoCabinet;
use strict;
use warnings;

my $fbase = $ARGV[0];
my %idx;
if (! tie (%idx,  'TokyoCabinet::HDB', "$fbase.idx.tch", TokyoCabinet::HDB::OWRITER | TokyoCabinet::HDB::OCREAT, 16777213, -1, -1, TokyoCabinet::TDB::TLARGE, 100000)){
	die "cant open $fbase.idx.tch\n";
}else{
}
open IDX, "$fbase.idx";
while(<IDX>){
	chop();
	my ($i, @p) = split(/\;/, $_, -1);
	if (!defined $idx{$p[$#p]}){
	  	$idx{$p[$#p]} = $i;
	}else{
		if ($idx{$p[$#p]} != $i){
			print STDERR "mismathed ids $i new, $idx{$p[$#p]} old for $p[$#p]\n";
		}
	}
}
untie %idx;
