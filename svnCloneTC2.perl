use strict;
use warnings;
use line;
#########################
# create code to versions database
#########################
use Compress::LZF;
use TokyoCabinet;

sub enc {
	my $str = $_[0];
	$str =~ s/\[/\%5B/g;
	$str =~ s/\]/\%5D/g;
	$str =~ s/\#/\%23/g;
	$str =~ s/\%/\%25/g;
	$str;
}

my $repo = $ARGV[0];
my $fbase="$ARGV[1]";
 
my $fname=$fbase.".tch";
my %clones;
if(!tie(%clones, "TokyoCabinet::HDB", "$fname", TokyoCabinet::HDB::OWRITER | TokyoCabinet::HDB::OCREAT)){
      print STDERR "tie error for $fname\n";
}
my $idx = -1;
my %seen;
open IDX, "$fbase.idx";
while(<IDX>){
	chop();
	my ($i, $p) = split(/\;/, $_, -1);
	$idx = $i if $i > $idx;
	$seen{$p}++;
}
open IDX, ">>$fbase.idx";
open IDXL, ">>$fbase.idx.last";
while(<STDIN>){
	chop();
	my $cmd = "$_";
	my $path = $_;
	$path =~ s/^.*\Q$repo\E//;
	$path =~ s/\"$//;
	$path =~ /^([^\/]*)\//;
	my @x = split(/\@/, $path, -1);
	my $vs = pop @x;
	$path = join "\@", @x;
	next if defined $seen{"$path/$vs"};
	if ($path ne ""){
		next if !($path =~ /$matchExt/);
		$cmd = enc($cmd);
		open B, "$cmd |";
		my $line = "";
		while(<B>){
			$line .= $_;
		}
		close(B);
		if ($line ne ""){
			my $cline = compress ($line);
			if (defined $clones{$cline}){
				my $ind = $clones{$cline};
            print IDX "$ind\;$path/$vs\n";
			}else{
				$idx++;
				$clones{$cline} = $idx;
				print IDX "$idx\;$path/$vs\n";
				print IDXL "$idx\n";
			}
		}
		my $size = length($line);
	}
}
close IDX;
close IDXL;
untie %clones;
