package line;

require Exporter;
our @ISA = qw (Exporter);
our @EXPORT = qw($matchExt $matchRealC $matchStorage cleanGen goodExt cleanFname cleanCMPath outputRpm  outputContent classifyLine fixExt project fixSCCSPath fixPath fixPathOld cleanTenovis cleanIntuity cleanExpertnet cleanPt cleanIcpj cleanCmapi cleanCCS  cleanIC cleanIvr cleanDialer cleanSmbs cleanCCR cleanCore cleanIptel cleanTns cleanWrs cleanMM);
use vars qw(@ISA);

sub cleanGen {
	my $dir = @_[0];
	$dir =~ s/^\///;
	my @x = split(/\@\@/, $dir);
	if (scalar(@x) > 2 || (scalar(@x) > 1 && $x[$#x] !~ /\/[0-9]*$/)){
		$x[1] =~ s/^\///;
		my @pp = ();
		while ($x[1] =~ /^([^\/]*)/){
			my $var = $1;
			if ($var =~ /^[0-9]+$/){
				$x[1] =~ s/^\Q$var\E\///;
				$x[1] =~ /^([^\/]*)/;
				$var = $1;
				push  @pp, $var;
			}
			if ($x[1] =~ /^\Q$var\E\//){
				$x[1] =~ s/^\Q$var\E\///;
			}else{
				last;
			}
		}
		$dir = "$x[0]/".(join '/', @pp);
		$dir =~ s/\/\.\//\//;
	}else{
		if (scalar(@x) == 2){
			$dir = "$x[0]";
		}
	}
	$dir;
}

#pre Dec 9, 2015
#our $matchExt = q/\.(erb|ASN|BAT|C|CPP|Cpp|DSP|DTD|F90|H|INI|Rd|S|TXT|XML|ada|adb|adc|adf|adl|ads|afm|am|asctxt|asm|asn|asp|aspx|atf|awk|bas|bat|bdf|c|c\+\+|cbs|cc|cce|ccd|cdf|cdl|cf|cfg|cgi|cgs|cif|clw|cmd|cp|cpp|cpps|cs|csh|csp|css|ct|ctl|cxx|d|dat|def|des|dfn|docbook|dsp|dsw|dtd|dtl|dtml|e|ec|el|erl|exp|f|fd|fig|for|frm|glade|h|hh|hhk|hi|hpp|hrc|hs|htf|htm|html|hxx|i|i3|icl|idl|inc|ini|iqd|ism|j|jav|java|java,|jpl|js|jsp|ksh|l|lang|lhs|lisp|lua|m|m2|m3|m4|man|map|mdl|mf|mib|mif|ml|mli|mo|msml|n|nasl|nc|nib|odl|p|p_hi|pas|perl|pfb|ph|php|php3|phpt|pl|pm|po|pod|pp|pt|py|qsc|rb|rc|rdf|resx|rul|rtf|rv|rvf|s|sce|scm|sed|sgml|sh|shape|shtml|sql|ss|st|sty|svg|t|tcl|test|tex|texi|tlx|tm|tpl|txt|ui|vb|vbp|vbs|vbe|wdml|wsdd|wsdl|xal|xcu|xhp|xlb|xml|xq|xpm|xsd|xsl|xslt|xul|y|yxx)$/;
#post Dec 9, 2015
our $matchExt = q/\.(pp|jelly|xib|f90|jade|rdoc|wsdl|ctp|sass|scss|phtml|vim|pde|csl|mustache|textile|rhtml|xht|xhtml|md|markdown|mdown|mkdn|mkd|mdwn|mdtxt|mdtext|text|json|clj|scala|go|rs|dart|module|coffee|as|groovy|yaml|haml|rst|cmake|rake|erb|ASN|BAT|C|CPP|Cpp|DSP|DTD|F90|H|INI|Rd|S|TXT|XML|ada|adb|adc|adf|adl|ads|afm|am|asctxt|asm|asn|asp|aspx|atf|awk|bas|bat|bdf|c|c\+\+|cbs|cc|cce|ccd|cdf|cdl|cf|cfg|cgi|cgs|cif|clw|cmd|cp|cpp|cpps|cs|csh|csp|css|ct|ctl|cxx|d|dat|def|des|dfn|docbook|dsp|dsw|dtd|dtl|dtml|e|ec|el|erl|exp|f|fd|fig|for|frm|glade|h|hh|hhk|hi|hpp|hrc|hs|htf|htm|html|hxx|i|i3|icl|idl|inc|ini|iqd|ism|j|jav|java|java,|jpl|js|jsp|ksh|l|lang|lhs|lisp|lua|m|m2|m3|m4|man|map|mdl|mf|mib|mif|ml|mli|mo|msml|n|nasl|nc|nib|odl|p|p_hi|pas|perl|pfb|ph|php|php3|phpt|pl|pm|po|pod|pp|pt|py|qsc|rb|rc|rdf|resx|rul|rtf|rv|rvf|s|sce|scm|sed|sgml|sh|shape|shtml|sql|ss|st|sty|svg|t|tcl|test|tex|texi|tlx|tm|tpl|txt|ui|vb|vbp|vbs|vbe|wdml|wsdd|wsdl|xal|xcu|xhp|xlb|xml|xq|xpm|xsd|xsl|xslt|xul|y|yxx)$/;
our $matchRealC = q/\.(C|CPP|Cpp|H|S|ada|asm|c|c\+\+|cc|cce|cp|cpp|cpps|cs|csp|cxx|def|ec|erl|f|for|h|hh|hhk|hpp|hrc|hs|hxx|j|jav|java|java,|lisp|m|ml|mli|pas|ph|s|ss|st|t)$/;
our $matchStorage = q/\.(tar|tgz|tbz|tar\.gz|tar\.bz2|zip)$/;

#
# pp is for puppet
# Jelly is an open and customizable XML processing engine
# Script written in R, rez resource compiler
# As of Interface Builder version 3, a new file format (with extension .xib) has been added, which is functionally identical to .nib, except it is stored
# The GYP file extension is associated with the Google Chromium project. GYP files contain input configuration data that are used to generate output files appropriate to the platform:
# jade - proprietary pl from NZ
# The EBUILD file type is primarily associated with 'Gentoo Linux'
# dart Pl or a bunch of other stuff
# The .td file extension is used for Teledisk archive files.
# TD is a file extension for a terrain file used with Microsoft Train Simulator. 
# The gradle file extension is associated with the Android Studio The gradle file stores data used to build app for Android.
# Developer file written in Smali, the assembly language used by the Android Dalvik Virtual Machine; usually created by decompiling .DEX (Dalvik Executable) files
# Document created by RDoc, a Ruby documentation application; contains documentation information in RDoc markup language that is compiled in HTML to document Ruby projects online. 
# CakePHP view files are written in plain PHP and have a default extension of .ctp
# The WSDL file type is primarily associated with 'Web Services Description Language'.
# File created by Twig, a PHP optimizing template engine
# ac Automatic configuration script produced by GNU Autoconf, or AC3D format used by AC3D, an application used to create 3D models for game
#  .sln file extension are most commonly associated with the Microsoft Visual Studio software application
# feature file extension is associated with Microsoft Visual Studio
#dtsi - device tree?
#scss - css?
#Sass is an extension of CSS3, adding nested rules, variables, mixins, 
#pot - powerpoint
#phtml was the standard file extension for PHP 2
#NPMIGNORE file extension is associated with the Git 
#expectf - php tests
#The NIX file type is primarily associated with 'Rational XDE' by IBM. 
#xht,xhtml Web file written in XHTML
#The CLJ file type is primarily associated with 'Clojure' 
#The PLIST file type is primarily associated with 'Property List XML File'.
# ngdoc - angularjs doc
# JD is a file extension associated with Jigsaw Terminal files. I
# gemspec RubyGems a package manager for the Ruby 
# DM files are generally audio files encoded with DRM
# mustache project file
# csl Citation Style Language
# PDE Processing programming language
# PBXPROJ is an Xcode data file that saves project data such as the list of source code, resource files, and build properties
# DTS encoded audio file 
# textile - markup
# module - drupal
# md|markdown|mdown|mkdn|mkd|mdwn|mdtxt|mdtext|text - markdown
# scala|go|rs languages
# coffee|as|groovy|yaml|haml scripts/markup
# rst - reStructuresd text
# cmake|rake - make
# ts - video
# mm - mindmap
# The RHTML file type is primarily associated with 'Ruby on Rails'
# .F90 probably fortran90 intagm.F90
# .Po - some generated dependencies
# .ac configure.ac
# .ada - looks ile ada src/gcc/gcc/testsuite/ada/acats/support/checkfil.ada
# .adb gnat compiler component
# .ads glade component
# .afm  StartFontMetrics 2.0
# .am used for makefiles
# .bdf star font
# .beam - a binary format
# .bl linuxphobia-1.1/pics/help_bigline.bl
# .bodies bouml_2.19.2/plugouts/rose/7169.bodies
# .boo boo-0.7.6.2237/tests/testcases/stdlib/map-1.boo
# .cgs testcase ldrh.cgs
# .cif gas-sensor.cif
# .csi lincity-1.13.1/icons/waterd.csi
# .cur cursor
# .de webmin-1.250/sshd/config.info.de
# .dfn Ultima Offline eXperiment 3  dfndata/npc/mounts.dfn
# .docbook son xml like ksimus-boolean-0.3.6/doc/en/boolean-timing.docbook
# .dtl otrs/Kernel/Output/HTML/Standard/Notify.dtl
# .dtml COREBlog/dtml/trackbackResult.dtml
# .e3d 3dobjects/weapons/sword7.e3d
# .enc - encoding tcl8.4.11/library/encoding/iso8859-8.enc
# .epm some model .xblast-2.10.0/image/sprite/cowboy_R_3.epm
# .erl may be erlang c17m/src/chat/chat.erl
# .es config.info.es
# .expected some gis or xml stuff
# .fd font stuff ucs/contrib/letgfzem.fd
# .fig  - xfig file? 
# .fish fish-1.22.1/share/functions/popd.fish
# .glade xml win_keys.glade
# .glf gtklife-5.1/patterns/lpa/synthesis/make_osc.glf
# .gmo - world lang/key bind OroboROX/Configure/Keybindings/Messages/de.gmo
# .gmo some world language thing
# .gsm - sound
# .guess
# .hi not clear dfhc98-1.0beta/include/NonStdTrace.hi
# .hrc OO resources in C
# .hs - haskel
# .htf - tex font related mtmi-bs.htf	
# .i3 ??? Protocol.i3
# .i3 corba like stuff m3-comm/events/src/EventStubLib.i3
# .icl Definitions for visual interface
# .ico xrick-021212/src/xrickST.ico
# .ics mozilla/other-licenses/libical/zoneinfo/Pacific/Wake.ics
# .id - some derived file fakeroot-1.0.7/.arch-ids/simple.c.id
# .info infomodule.info config.info
# .l - lexer
# .lang highlight syntax perl.lang
# .level - game level
# .lhs ghc-6.0.1/libraries/base/GHC/Stable.lhs
# .lkr linker command file gputils-0.13.3/lkr/hcs1370.lkr 
# .lst - some library listing?? src/binutils/ld/nios2_ld_xfail.lst
# .lua paint level_paint.lua
# .m camal ReconItem.m
# .m2 Macaulay2-0.9.95/Macaulay2/packages/binomialPD.m2
# .mf metafont
# .mli  Objective Caml  myMap.mli
# .mn jar.mn
# .mo scilab  VVsourceAC.mo
# .msml some xml for mars rover
# .n troff return.n
# .nasl nessus script acc.nasl
# .nib xml bumprace-1.5.1/macosx/SDLMain.nib/info.nib
# .ogg - sound
# .ott OO resume.ott
# .p_hi imports/Text/ParserCombinators/ReadP.p_hi
# .pamphlet - texed version of source axiom/src/lib/wct.c.pamphlet 
# .pdu lsh-2.0.1/src/testsuite/rapid7-ssh-pdu/0000666.pdu
# .pfb scalable fonts used in the PDF version tipa-1.3/type1/tipa10.pfb
# .ph - header of the c file cc_display.ph
# .po - some locale specific stuff
# .pod - perl
# .pot regexxer.pot
# .pp nuttest.pp
# .ppd some printer desc file wine-0.9.9/dlls/wineps/generic.ppd
# .pro  looks like project file qt-x11-opensource-src-4.2.1/projects.pro
# .pt xml CMFMember/skins/cmfmember/memid.pt
# .pyc - binary
# .pyo - python object format? ./usr/lib/python2.4/distutils/debug.pyo
# .qci mpqc-2.3.1/src/bin/mpqc/validate/ckpt/energy.qci
# .rb - ruby
# .rc some resource scrollsub.rc
# .rd some tex like language xyVector.Rd
# .rdf xml/rdf redland/raptor/tests/rdfs-namespace.rdf
# .rv some xml for mars rover
# .rvf some xml for mars rover
# .sce - scilab-4.0/macros/signal/buildmacros.sce
# .sci - scilab packages/scilab/linux/Scilab/demos/dae/pendg.sci
# .sgf - som game layout
# .shape - sybase xml Dia/Sybase/client.shape 
# .sip python - PyKDE-3.11rc1/sip/kio/kurlpixmapprovider.sip
# .sln project file? mkvtoolnix-1.8.0/mkvtoolnix.sln
# .sml - SML/NJ plist.sml
# .spec siproxd-0.5.13/siproxd.spec
# .ss lisp plt/collects/errortrace/errortrace.ss
# .st smaltalk StringCollection.st
# .sub config.sub config.guess
# .test - test control
# .tfm - binart tex font
# .tga - some game related file 
# .tm font descriptions for TeXmacs font.tm
# .tpm - tex format texmf-dist/tpm/rplain.tpm
# .ttf fonts
# .ui some xml like - cppcodegenerationformbase.ui
# .uu uuencoded binary
# .vf - binary texmf/fonts/vf/public/cm2cs/cmb10.vf
# .x - some crap sendmail-8.12.10/devtools/OS/NEWS-OS.6.x
# .xal XBlast TNT level Ahahahah2.xal
# .xal xblast-2.10.0/level/Arena.xal
# .xba - OO OOO_2_0_4/qadevOOo/tests/basic/ifc/awt/XControl/awt_XControl.xba
# .xcu - openoffice 
# .xhp OO
# .xlb OO
# .xpm X image in c UFOsightings/restoreI.xpm
# .xq -xquery
# .xul mozilla xml printPageSetup.xul


#in Avaya
#.am makefile vobs/SanityChecker/.@@/main/SANITYCHECKER_1_0_DELIVERY/arankine_build_rpm_experiment/1/Makefile.am@@/main/arankine_build_rpm_experiment/2
#.aspx asp /vobs/aci/AOK/AOK.WebUI/addbookmark.aspx@@/main/7
#.adc,adl,adf - spme sort of script #/vob1/qwsrc/client/ccq/ccq.adc #/vob1/qwsrc/client/bankq/bankq.adl
#.cdl - xml #/vob1/qwsrc/qconsole/VQS/L10N/zt_TW/avaya_agent_vqs_zt.cdl
#.cf, clw, some sort of config
#.cfg # xml file in /thinclient/src/java/sdk/client/agitar/config/ag-default.cfg # some prject file in vob6/webcenter/C++/projects.cfg # Alliance Generation Configuration in /vobs/.its_vob1/conf/pstn/e1fraisdn.cfg
#.clw - CLW file contains information for the MFC ClassWizard #/vob4/source/client/ActiveXPhone/GuiControls/TestSoftphone/TestSoftphone.clw@
#.csp c++ #/audix/ax32/sable/_cs7/_dup/sdb/plt/websrv.ss/definityTools.fn/cgi-bin.m/s.unsec_login.csp
#.css content style sheet
#.data some script #/nj/sablime2/sdb/wchta/src/cti/ot/lib.otlib/s.ConnectionID.data;
#.des looks like troff
#.dsp - Microsoft Developer Studio Generated Build File /vobs/bcmrd/BCMSVu/bcmsvuserver/serverinterface/serverinterface.dsp
#.df - cms/intuity UI spec script file /dr/sablime2/sdb/cms/cmsdb.ss/gem.m/dict.dm/s.d_custom.df
#.dsw - Microsoft Developer Studio Workspace File
#.dsw - Microsoft Developer Studio Workspace File# /nj/sablime2/_ipsof/sdb/ipsof/pwsp32/mixed/s.pwsp32m.dsw
#.ec - c++ #sablime2/sdb/31cms/dbm.ss/idbm.p/informix.d/s.misc.ec
#.frm - some vb form /vob1/qwsrc/client/template/core/frmrptsd.frm or: /audix/ax32/sable/_da/sdb/imapi/voice.ss/mm.fn/_win.tl/s.frm_phon.frm
#.hhk - html #/audix/ax32/sable/_da/sdb/web/Admin/WebHelp/Enterprise/s.WHstart.hhk
#.i ?? #/audix/ax74/sable/_ia/sdb/ia/zpkg.ss/snmpoff.pkg/s.S80lp.i
#.idl - some c'like corba stuff IDL source for QuintusTreeCtrl# /vob1/qwsrc/fw/lib/qtree/qtreectrl.idl
#.inc vbscript #/vob6/webcenter/Distribution/asp/info/include/config.inc
#.ini ?? #/nj/sablime3/sdb/plat/platform.test/hi/s.oclfull.ini
#.jpl - JAM screen entry function: script #/audix/ax32/sable/_cs8/sdb/nlcr/nlcr/src/proxy/screens/s.deleteentry.jpl
#.mdl - looks like corba stuff #/nj/sablime2/_ipsof/sdb/ipsof/ipsr2/LTLdap/s.LTLdap.mdl but also a bit strange /vob1/qwsrc/client/ctiq/reports/contacts/contacts.mdl
#.odl - c++ like - basic? #/audix/ax32/sable/_da/sdb/imapi/libs.sb/mm0405.fn/_win.tl/s.mm0405.odl
#.pam - some bs: /nj/sablime2/sdb/pacs/PAM/s.28.pam
#.pas - pascal #/vobs/ivr_vw_win/sodd/core/gridmgr.pas
#.qsc - qscript #/vob1/qwsrc/archive/client/common/qprompter.qsc
#.rc - Microsoft Developer Studio generated resource script /nj/sablime2/_ipsof/sdb/ipsof/Softphone/PoP/a2420display/s.a2420display.rc
#.ref - looks like some content file, not a lang  /dr/sablime/_dsnmp/sdb/dsnmp/appl_fls/guide_pa/index/s.idx00028.ref
#.rul #/audix/ax32/sable/_da/sdb/imapi/voice.ss/mmsetup.fn/_win.tl/basic.d/working/s.setup.rul
#.t - looks like c/cpp #/audix/ax32/sable/_cs7/_dup/sdb/plt/prism.ss/alerter.fn/mkAlerter.p/s.AlertInc.t
#.tpl - C'like state language /audix/ax32/sable/_cs7/sdb/cs/intuity/src/tele/t1/t1pack/8751RAM/SSM/V4/s.st_tst.tpl
#.type - som boring stuff #/audix/ax32/sable/_cs7/_dup/sdb/plt/swchIntPkg.ss/teladm.fn/data/switches/default/default/s.add3.type
#.vbp visual basic project

# .pdb - binary - Microsoft C/C++ program database 2.00/vobs/IPA_R4.0/Release_Versioned/sipua.pdb@@/main/1
# .ccd - xml for ccd system /external/rs6k/rogue/sourceproed8@@/main/aic71x_integration/1/RogueWave/main/aic71x_integration/1/SourcePro/main/aic71x_integration/1/Ed8/main/aic71x_integration/1/records/main/aic71x_integration/1/libraries/main/aic71x_integration/1/stdlib/main/aic71x_integration/1/ch13n/main/aic71x_integration/1/sig_atomic_t.ccd@@/main/aic71x_integration/1
# .dat - shell variables -- /vobs/ivr/ENV.dat@@/main/ivrintg_060830_IR3.0/10
# .mib - Example MIB for the delivered attributes.xml/vobs/redhat_jboss_rel/release/extint/jboss/server/all/deploy/snmp-adaptor.sar/attributes.mib@@/main/1
# or /vobs/.boxster/boxster.brd@@/main/overlan_R71/ysabbah_031113_itc_delivery_01_00_03/1/ccg/main/overlan_R71/ngurfink_050309_itc_delivery_25_2_50/1/mib/main/overlan_R71/ngurfink_050309_itc_delivery_25_2_50/1/prv/main/overlan_R71/ngurfink_050309_itc_delivery_25_2_50/1/src/main/overlan_R71/sasson_061123_ITC_4_1_Delivery_27_1_55/1/pbnac_g450_cnf.mib@@/main/overlan_R71/1

#.vm - html - /vobs/ivr/air.pj/java.ss/webadm.fn/data.m/template/voipchg.vm@@/main/7
#  or preprocessor /vobs/redhat_jboss_rel/release/extint@@/main/1/docs/main/1/examples/main/1/varia/main/1/deployment-service/main/1/templates/main/1/VM_global_library.vm@@/main/1
#.fm may be framemaker file /vob2/Docs/7.xDocs@@/main/18/test/main/1/webadmin_preface.fm@@/main/1
#.uip installer config /vobs/diamond2/dublin/dev/install/diamond-dublin.uip@@/main/8
#.atf xml /vobs/diamond2/dublin/dev/atf/scripts/email.atf@@/main/2
#.wsdl xml /vobs/ccr/datainteg/conf/dataexport/DataExport.wsdl@@/main/2
#.wsdd xml/vobs/coreservices_rel/release/cs_alarming/drop/sdk/data/cs_AlarmServer_deploy.wsdd@@/main/1
#.frx binary VB? /v_dialerclient/Redwood/VBApps/Campaign Monitor/childTemplateWeb.frx@@/main/1
#.resx MS ResX schema /thinclient/src/csharp/sample/Com.Avaya.Ic.Sdk.Sampleclient/Ui/frmAddressBook.resx@@/main/1
#.ism xml /vobs/Common/.@@/main/Packet1_fra/2/Installation/main/Packet1_fra/ADNWEB_E062_fra/9/ADN-WEB/main/ADNWEB_E062_fra/WBM_fra/8/WbM.ism@@/main/WBM_fra/41
#.arcf some report format /v_dialerclient/Redwood@@/main/cd4.0/16/Language/main/cd4.0/2/Campaign Analyst/main/cd4.0/10/chinese_simplified/main/cd4.0/4/apds-adminROLLUP.arcf@@/main/dev3/1
#.xslt 
#.asn some macro /vobs/pt.pj/protocols.ss/ip.f/h323.f/elemedia.b/ex3230s/asn1/h225asn1/em_h225.asn@@/main/1
#.iqd COGNOS query /vobs/oa.pj/packages/common/p_rept/reports/cognos/cubes/CallCenterQ/oracle/category.iqd@@/main/2
#.ilk - binary - /external/win32/rogue/sourceproed8/lib/trace2315d.ilk@@/main/1
#.UNC - binary - /vobs/mm_wso_ui_temp/AVP/avp/lang/avpzh-TW.UNC@@/main/2
#.fdp - installation related - vobs/cstone/cstone.pj/zpkg.ss/AUDIXset.pkg/rmv_pkgs.fdp@@/main/1
#.vbw - strange vb related parameters? - /v_dialerclient/Redwood/VBApps@@/main/dev3/2/Health Monitor/main/dev3/2/HealthMonitor.vbw@@/main/dev4/1
#.vb VB /vobs/ecad_quality_center/reports/QCReports/QQTDReport.aspx.vb@@/main/3
#.ASN some script?? /vobs/MVAPBase/ApiServers/TSAPI/Tserver@@/main/1/ASN/main/28/SDB.ASN@@/main/3
#.jam Altera Jam/STAPL Composer Version 10.2 - app_prgm_pld_dcp.jam

sub goodExt {
	my $f = @_[0];
	#$f =~ /$matchExt/;
	1;
}

sub cleanFname {
	my $f = @_[0];
	$f =~ s/\,v$//;
	$f =~ s/^s\.//;
	$f;
}

sub outputContent {
	my ($f, $d) = @_;
	my @x = split(/\//, $f, -1);
	my $file = pop @x;
	my $dir = join '/', @x;
	
	my $cmd = "tar tf $f |";
	$cmd = "bunzip2 < $f | tar tf \- |" if $file =~ /\.(tar\.bz2|tbz)$/;
	$cmd = "gunzip < $f | tar tf \- |" if $file =~ /\.(tar\.gz|tgz)$/;
	$cmd = "unzip -l $f | awk \"".'{ print \$NF}'."\" |" if $file =~ /\.zip$/;

	print STDERR "$f\;$cmd\n";
	
	open B, $cmd;
	while (<B>){
		chop();
		next if /^(Name|----|-------|files)$/;
		my @xx = split(/\//, $_, -1);
		my $ff = pop @xx;
		my $dirx = join '/', @xx;
		$ff = cleanFname ($ff);
		#print "$ff\n";
		if (goodExt($ff)){
			print "$dirx/$ff\n";
		}
	}
}

sub outputRpm {
	my ($f, $d) = @_;
	my @x = split(/\//, $f, -1);
	my $file = pop @x;
	my $dir = join '/', @x;

	my $dir = "/tmp/$$";
	my $i = 1;
	while (mkdir("$dir") == 0){
		$dir = "/tmp/".($$ + $i);
	}
	
	my $cmd = "cd $dir\;rpm2cpio $f | cpio -it|";
	open B, $cmd;
	my %fs;
	while (<B>){
		chop();
		$fs{$_}++;
	}
	foreach my $f1 (keys %fs){
		my $cmd = "cd $dir\;rpm2cpio $f | cpio -i $f1|";
		if ($f1 =~ /$matchStorage/){
			print STDERR "- $f1\;$cmd\n";
			open B, $cmd;
			while(<B>){};
			outputContent ("$dir/$f1", "$dir");
		}else{
			#print "$f\;$f1\n";
		}
	}
	system ("rm -rf $dir"); 
}
sub cleanCMPath {
	my $f = @_[0];
	$f =~ s/^\/dr\/defty\/defty2\/tools\/tools\.pj\///;
	$f =~ s/^\/dr\/defty\/r[0-9\.]*\/(dev|int|dot)\/[^\/]*\///;
	$f =~ s/^\/dr\/defty\/custom1[0-9]\/(dev|int|dot)\/[^\/]*\///;
	$f =~ s/^\/usr\/add\-on\/(defty|legacy)\/([cmvr0-9\.]*|daily_test)\/(int|dev|dot)\/[^\/]*\///;
	$f =~ s/^\/usr\/add\-on\/legacy\/r9dot\/r9.1\/[^\/]*\///;
	$f =~ s/^\/usr\/add\-on\/cmlp\/(M001|field_base)\/[^\/]*\///;
	$f =~ s/^\/usr\/add\-on\/grr\/[^\.]*\.pj\///;
	$f;
}

sub fixExt {
	my $ext = @_[0];
	my $res = "Other";
	$res = "C" if ".$ext" =~ /\.(C|CPP|Cpp|H|cc|cp|cpp|cpps|cs|cxx|hpp)$/;
	$res = "c" if ".$ext" =~ /\.(c|h|ec|def|t)$/;	
	$res = "Java" if ".$ext" =~/\.(java)$/;
	$res = "Web" if ".$ext" =~/\.(jsp|js|asp|aspx|php|wsdl|pl|pm|py|tcl)$/;
	$res = "Script" if ".$ext" =~/\.(BAT|adf|adc|adl|awk|bas|bat|cmd|csh|csp|ct|df|inc|jpl|ksh|mdl|odl|pl|psql|qsc|rul|sed|sh|sql|tpl|vb|vbs|y|yxx)$/;
	$res = "Html" if ".$ext" =~/\.(htm|html|xml|XML|dtd)$/;
	$res;
}

sub fixSCCSPath {
	my $f = @_[0];
	my @x = split (/\//, $f, -1);
   $x[$#x] =~ s/^s\.//;
	$f = join '/', @x;	
	
	$f =~ s/^\.//;
	$f = "/$f" if $f !~ /^\//;
	$f = "/dr$f" if $f =~ /^\/sablime/;
	$f =~ s/\/sdb\//\//g;		
	$f =~ m/\/_([^\/]*)\/([^\/]*)\//;
  	my ($p,$p1)=($1,$2);
  	$f=~s/\/_$p\/$p\//\/$p\// if ($p eq $p1);
	$f =~ s/\/cs\.bk\//\/cs\//;

	my ($project, $newf) = project($f);
	#print STDERR "$project\;$newf\n";
	
	if ($project eq ""){
		$f=~ s/^\///;			
		$f=~s/^dr\///g;
		
		$f=~s/sdb\///g;
		$f=~s/sdb\///g;
		$f=~s/^sablime\/|^sablime[0-9]\///;
		$f=~ s/^nj\/sablime[0-9]\///;
		$f=~ s/^audix\/ax(32|74)\/sable\///;
		#cms
		$f=~s/^31cms\/|^3ncms\/|^3rcms\/|^96cms\/|^cms\/|^cms8p\/|^ucms\/|^v3cms\/|^v3cms\.pre_cleanup\/|^v5cms\/|^v6cms\/|^v7cms\/|^v8cms\/|^v9ccp\/|^v9cms\/|^cms.pj\//CMS\//;
		#deal with various stuff in nd database
		$f=~s/^bri\/|^cala\/|^cbmri\/|^cb\/|^cpu\/|^goats\/|^hkpr1\/|^i15\/|^l2\/|^l3\/|^l4\/|^l4x\/|^l61\/|^l6\/|^l7\/|^l8\/|^lpin\/|^m21\/|^m22\/|^m23\/|^m2\/|^m4\/|^mli\/|^pacs\/|^quest\/|^xp1\/|^x\//misc\//;
		#deal with gateway
   	$f=~s/^_defhw\/75hw\/[^\.\/]*\.pj\//TNS\//;
   	$f=~s/^_ifw\/[0-9]+\//TNS\//;
   	$f=~s/^_ifw\/iplay\/[^\.\/]*\.pj\//TNS\//;
   	$f=~s/^wiz2\/angel\//TNS\//;
		#deal with IA
   	$f=~s/^.+\/intuity\//intuity\//;
		$f=~s/^_drvex\/|^MsgCore\/|MsgCore\.pj\/|^victest\.pj\//intuity\//;
   	#$f=~s//intuity\//;
   	$f=~s/^(da|_da|ia|_cs[0-9]|_PLAT)\//intuity\//;		
	
		#???
   	$f=~s/rdiag\/pcpacs[0-9]+\/pcpacs[0-9]+\//rdiag\//;
   	$f=~s/pacsradmin\//admin\//;
	
		$f=~s/^ipt1\//IPT\//;
		$f=~ s/^_defhw/TNS/;
		$f=~ s/^_intaud/intuity/;
	}else{
		$f = "$project/$newf";
	}
	$f;
}

our @sablime_databases = (
								  #my additions
	[ 'services','/nj/sablime2/quest',    '*',    'quest', 'ms', 'QUEST' ],
	[ 'csd', '/dr/sablime/satpc', '*',    '*', '*', 'SATPC' ],
	[ 'csd', '/dr/sablime2/_ifw', '*',    '*', '*', 'TNS' ],
	[ 'csd', '/nj/sablime1/wiz2', '*',    '*', '*', 'TNS' ],
	[ 'ecad', '/dr/sablime[0-9]*/[0-9vnr]*cms', '*',    '*', '*', 'CMS' ],
	[ 'ecad', '/dr/sablime2/v3cms.pre_cleanup', '*',    '*', '*', 'CMS' ],
	[ 'ecad', '/dr/sablime2/(ucms|cms8p|v9ccp)', '*',    '*', '*', 'CMS' ],
	[ 'smbs', '/nj/sablime2/l[0-9]+',    '*',    '*',     'ms', 'Legend' ],
	[ 'smbs', '/nj/sablime2/mli',    '*',    '*',     'ms', 'Legend' ],
	[ 'smbs', '/nj/sablime2/x',    '*',    '*',     'ms', 'Legend' ],
	[ 'smbs', '/nj/sablime[23]',     'm[0-9]*',   '',     'm',  'Legend' ], #Magix
	[ 'ecad', '/dr/sablime[0-9]*/_intaud/(LZVEX|cbaro)', '*',    '*', '*', 'intuity' ],

	[ 'csd',     '/dr/sablime2/_defhw','*', '75hw',  'm',  'TNS' ], #media gateway
								  
	[ 'ecad','/usr/add-on/proedir/sable_db/_ebs',     '*','*','ms','intuity' ],  
	[ 'ecad','/_drvex/DRVEX',    '*','*','ms','intuity' ],  
	[ 'ecad','/_drvex/isd',    '*','*','ms','intuity' ],  
	[ 'ecad','/_drvex/vplay',    '*','*','ms','intuity' ],  
								  #changed these

	[ 'ecad','/audix/ax74/sable/ia',     '*','*','ms','intuity' ],  
	[ 'ecad','/audix/ax32/sable/da',     '*','*','ms','intuity' ],  #message manager
	[ 'ecad','/audix/ax32/sable/_cs[0-9]','*','*','ms','MM' ],  
	[ 'ecad','','_serenade','*','ms','Serenade' ],
	[ 'ecad','','_aria','*','ms','Aria' ],
	[ 'ecad','/_mn','*','mn','ms','intuity' ], #Message Networking
	[ 'ecad', '',	'_mmser',	'*',	'm', 'MM' ],
	[ 'ecad', '',	'_mmaud',	'*',	'm', 'MM' ],
								  
	[ 'cad',     '/nj/sablime2/ipt1',  '*',  'ipt1',  'ms', 'IPT' ],
	[ 'cad',     '/nj/sablime2/ipsof', '*', 'ipsof', 'ms', 'IP_SoftPhone' ],
	[ 'cad',     '/dr/sablime2/ipsof', '*', 'ipsof', 'ms', 'IP_SoftPhone' ],
	[ 'ecad',    '/sable/sable_db/aria', '*',  '*',  'm',  'Aria' ],
	[ 'ecad','/dr/sablime2/_IC',   '*',    '*','ms','MM' ],
	[ 'ecad','/nj/sablime3','asai','asai','ms','ASAI' ],
	[ 'csd', '/dr/sablime',	'dsnmp',	'dsnmp',	'm', 'IMS' ],

								  
   #  PH         File System        Release  Generics MR/SRC Directory Name
	[ 'smbs',    '/nj/sablime2',    'gdb',    '*',     'ms', 'Partner' ],		# Need to figure Partner out!
	[ 'smbs',    '/nj/sablime2',    '_pvm',   'pvm',   'ms', 'Partner_Voice_messaging' ],
	[ 'services','/dr/sablime',     '_tdil',  'TDIL',  'ms', 'Ticket_Data_and_Interface_Layer' ],
	[ 'services','/nj/sablime1',    '_ssg',   'SSG',   'ms', 'Secure_Services_Gateway' ],
	[ 'services','/nj/sablime1',    '_ssdp',  'SSDP',  'm',  'Secure_Services_Delivery_Platform' ],
	[ 'services','/nj/sablime2',    '_SeS',   'SeS',   'ms', 'Installation_Security_Screening' ],
	[ 'services','/dr/sablime1',    '_rmb',   'rmb[t2]','ms','Remote_Maintenance_Board' ],
	[ 'services','/dr/sablime1',    '_mt',    'quest', 'ms', 'QUEST' ],
	[ 'services','/dr/sablime1',    '_mt',    'pv1',   'ms', 'Product_Viewer' ],
	[ 'services','/dr/sablime1',    '_mt',    'pips',  'ms', 'Product_Introduction_Presentation_System' ],
	[ 'services','/dr/sablime1',    '_mt',    'pie',   'ms', 'Product_Information_Expert' ],
	[ 'services','/dr/sablime1',    '_audexp','pcsxp', 'm',  'Password_Change_System' ],
	[ 'services','/dr/sablime1',    '_mvv',   'mvv',   'ms', 'Multi_Voice_Vendor' ],
	[ 'services','/dr/sablime1',    'mt',    'mt',    'ms', 'Message_Tracer' ],
	[ 'services','/dr/sablime3',    'mt',    'mt',    'ms', 'Message_Tracer' ],
	[ 'services','/dr/sablime1',    '_mt',    'iscrn', 'm',  'iscnn' ],
	[ 'services','/nj/sablime1',    '_exnet', 'exnet', 'ms', 'Expert_Net' ],
	[ 'services','/dr/sablime1',    'exnet', 'exnet', 'ms', 'Expert_Net' ],
	[ 'services','/dr/sablime1',    '_mpat',  'des',   'ms', 'Data_Expert_System' ],
	[ 'services','/dr/sablime1',    '_mt',    'cre',   'ms', 'Credit_Risk_Expert' ],
	[ 'services','/dr/sablime1',    '_mpat',  'cirs',  'm',  'Customer_Readiness_Infrastructure_Survey' ],
	[ 'services','/dr/sablime1',    '_mt',    'cirr',  'm',  'Customer_Inventory_and_Record_Reconcilliation' ],
	[ 'services','/dr/sablime1',    '_mpat',  'ces',   'm',  'Converged_Expert_System' ],
	[ 'services','/dr/sablime1',    '_mt',    'art',   'm',  'art' ],
	[ 'services','/dr/sablime/_args',  '*',   'ARGS',  'm',  'Automatic_Report_Generation' ],
	[ 'services','/nj/sablime3/_XUI',  '*',   'XUI',   'm',  'XUI' ],
	[ 'research','/nj/sablime3/_XUIR', '*',   'XUIR',   'm',  'XUIR' ],
	[ 'csd',     '/nj/sablime3/_IMPrs','*', 'IMPrs', 'm',  'CCS' ],
	[ 'csd',     '/dr/sablime/_def',    '*',    'defsw', 'm',  'CM' ],
	[ 'services', '/nj/sablime3',	'_SC',	'SC',	'm', 'Sanity_Checker' ],
	[ 'services', '/nj/sablime3',	'_GMS',	'GMS',	'm', 'Global_Managed_Services' ],
	[ 'ecad', '/dr/sablime',	'_nmsi',	'nmsi',	'm', 'Network_Management_System_Integration' ],
	[ 'csd', '/dr/sablime',	'_entnm',	'entnm',	'm', 'Enterprise_Network_Management' ],
	[ 'csd', '/dr/sablime',	'_cmlp',	'cmlp',	'm', 'Communication_Manager_LiverPool' ],
	[ 'csd', '/dr/sablime',	'_MVE',	'MVE',	'm', 'MultiVantage_Express' ],
	[ 'csd', '/dr/sablime',	'_csm',	'csm',	'm', 'Chain_Store_Management' ],
	[ 'services', '/dr/sablime1',	'_SAFE',	'SAFE',	'm', 'Software_and_Firmware_Entitlements' ],
	[ 'services', '/dr/sablime1',	'_mt',	'mt',	'm', 'Customer_Inventory_and_Record_Reconcillation' ],
	[ 'csd', '/dr/sablime2',	'_Malibu',	'Malbu',	'm', 'Malibu' ],
	[ 'services', '/dr/sablime2',	'_OSM',	'OSM',	'm', 'On_line_Service_Manager' ],
	[ 'csd', '/dr/sablime/_def',   '*',   '*',     'm',  'CM' ],
	[ 'csd', '/nj/sablime3/_IMPrs','*',   '*',     'm',  'CCS' ],
	[ 'csd', '/dr/sablime/_dsnmp', '*',   '*',     'm',  'IMS' ],
	[ 'csd', '/dr/sablime/_def',   '*',   '*',     'm',  'IMS' ],
	[ 'csd', '/dr/sablime/_ncm',   '*',   '*',     'm',  'IMS' ],
	[ 'csd', '/nj/sablime1/_mvsa', '*',   '*',     'm',  'IMS' ],

);


our (%f2p, %p2grp);
foreach $database (@sablime_databases){
	($product_area, $file_system, $release, $generic, $type, $product) = @$database;
	my $pref = $file_system;
	if ($release ne "*"){
		$pref .= "/$release/" ;
	}else{
		$pref .= "/";
	}
	$f2p{$pref} = $product;
	$p2grp{$product}=$product_area;
}


sub project {
	my $f = @_[0];
	my $res = "";
	foreach my $pref (keys %f2p){
		#print STDERR "$pref\n";
		if ($f =~ /^$pref/){
			$res = $f2p{$pref};
			$f =~ s/^$pref//;
		}
	}
	($res, $f);
}

sub fixPath {
	my ($f, $p) = @_;
	$f=~ s/^\.//;
	$f=~ s/^\///;
	if ($p eq "mm"){
		$f = cleanMM($f);
	}
	if ($p eq "v_vobs" || $f =~ /^v_/){
		$f = cleanDialer ($f);
	}
	if ($p eq "cvsdrnew"){
		if ($f =~ /^cmapi\//){
			$f = "CMAPI/$f";
		}
		if ($f =~ /^(coreservices|diamond|ispheres)\//){
			$f = "CORESERVICES/$f";
		}
		
	}
	if ($p eq "cvs"){
		$f = cleanCCS ($f);
	}
	if ($p =~ /^(avobs|nj)$/){
		$f = cleanGen ($f);
		
		$f =~ s/^vobs\/(Softphone|iClarity|PoP|PWSP32|SCAPI32|sp_Integrations)\//IP_SoftPhone\//g;		
		$f =~ s/^(vob[1-7]|thinclient|systest)\//IC\//;
		$f =~ s/^vobs\/bcmrd\//IC\//;


		if ($f =~ /^vobs\/ccs/){
			$f =~ s/^vobs\/(ccs|ccs_2test_comp1)\//CCS\//;
			$f =~ s/^vobs\/ccs_test[0-9]_comp\//CCS\//;
		}
		
		$f =~ s/^vobs\/(cstone|webclient|victest.pj|jcstest.pj|MsgCore|msgclients|intuity_[^\/]*)\//intuity\//;

		$f =~ s/^vobs\/(aesvcs|aesvcs_rel|cpSharedSvcs|comp_aesvcs1|MVAPBase)\//CMAPI\//;

		if ($f =~ /^vobs\/cms.pj/){
			$f =~ s/^vobs\/cms.pj\//CMS\//;
		}
		
		$f =~ s/^vobs\/(LAS|AS|GUI|Common|CM)\//TENOVIS\//;
		
		$f =~ s/^vobs\/(ivr_[^\/]*|ivr)\//IVR\//;

		$f =~ s/^vobs\/v_[^\/]*\/|^v_[^\/]*\//DIALER\//;
		
		$f =~ s/^vobs\/(victoryvs|geneva|tuiprompts|NotesSDK|PlatformSDK|UM[^\/]*)\//MM\//;
		$f =~ s/^(victoryvs|geneva|tuiprompts|NotesSDK|PlatformSDK|UM[^\/]*)\//MM\//;
		
		$f =~ s/^vobs\/\.its[^\/]*\//ITS\//;
		$f =~ s/^vobs\/\.(hdrs|[^\/\.]+\.ss)*\//OLD\//;
		
		$f =~ s/^vobs\/\.wrs[^\/]*\//TNS\//;		
		$f =~ s/^vobs\/deftyfw\.pj|^vobs\/\.(tn[xbdp0-9]*|flw_ryon|media_gateway|hummer|boxster)\//TNS\//;
		$f =~ s/^vobs\/(c[cs]_[^\/]*|viper|ispheres|diamond2|coreservices|coreservices_rel)\//CORESERVICES\/$1\//;

		$f =~ s/^vobs\/smbs_tango_com\/(tango|snode|home|demos|alsip)\//Tango\//;
		$f =~ s/^vobs\/smbs_[^\/]*\/IPO_[^\/]+\//IPOffice\//;

		$f =~ s/^vobs\/ccr\//CCR\//;

		$f =~ s/^vobs\/IPTel\//IPT\//;
		
		$f =~ s/^vobs\/pt\.pj\//pt\//;
		
		$f =~ s/^vobs\/ic\.pj\//intuity\//;
		
		$f =~ s/^vobs\/(eqm|cirs)\//Expert_Net\//;

		$f =~ s/^vobs\/(Spirit[^\/]*\/)/Spirit\/$1/;

		$f =~ s/^vobs\/(sourceforge_[^\/]*\/)/SourceForge\/$1/;

		$f =~ s/^vobs\/(sipcc[^\/]*\/)/SIPCC\/$1/;

		$f =~ s/^vobs\/(pan_[^\/]*\/)/PANTHER\/$1/;
		$f =~ s/^vobs\/(panther|panther_[^\/]*)\//PANTHER\//;

		$f =~ s/^vobs\/(acp_[^\/]*\/)/ACP\/$1/;

		$f =~ s/^vobs\/(asm-[^\/]*\/)/ASM\/$1/;
		
		$f =~ s/^vobs\///;
		$f =~ s/^(Serenade|Aria)\//OCTEL\/$1\//;
		$f =~ s/^apache_\//apache\//;
	}
	#now deal with the rest
	$f = cleanGen ($f);
	
	(split(/\;/, $f, -1))[0];
}

sub fixPathOld {
	my ($f, $p) = @_;
	$f=~ s/^\.//;
	$f=~ s/^\///;
	if ($p eq "mm"){
		$f = cleanMM($f);
	}
	if ($p eq "v_vobs"){
		$f = cleanDialer ($f);
	}
	if ($p eq "cmapi"){
		$f = "CMAPI/$f";
	}
	if ($p eq "ccs"){
		$f = cleanCCS ($f);
	}
	if ($p =~ /^(avobs|nj)$/){	  
		$f =~ s/\/(ccinteg|main|team)\//\//g;
		$f =~ s/\/(ccinteg|main|team)\//\//g;
		$f =~ s/\/(ccinteg|main|team)\//\//g;
		$f =~ s/\/mrs\-[^\/]*\//\//g;

		$f =~ s/\/(aic|columbus|ccadmin|rakesh|cclzsoft|fronczak|mvroman|paragp|aliahaid|dlk|tbb|tlz|jefw|deh|fred|rdmiller|ccbld01|japatel|baileyc|kure|dennisryan|sunkara|savant|adelmann|akhedkar|rsp1|ganbote|shivalkar|blondin|jhomer|montools|oellig|palrecha|team|ahuja|sstowell|davemac|latest|peacock|bld|karlo|kim|hkwalker|scottmcd|tkenny|nmeier|breight|clauson|befree|tjennings)_[^\/]*\//\//g;
		$f =~ s/\/(aic|columbus|ccadmin|rakesh|cclzsoft|fronczak|mvroman|paragp|aliahaid|dlk|tbb|tlz|jefw|deh|fred|rdmiller|ccbld01|japatel|baileyc|kure|dennisryan|sunkara|savant|adelmann|akhedkar|rsp1|ganbote|shivalkar|blondin|jhomer|montools|oellig|palrecha|team|ahuja|sstowell|davemac|latest|peacock|bld|karlo|kim|hkwalker|scottmcd|tkenny|nmeier|breight|clauson|befree|tjennings)_[^\/]*\//\//g;
		$f =~ s/\/(aic|columbus|ccadmin|rakesh|cclzsoft|fronczak|mvroman|paragp|aliahaid|dlk|tbb|tlz|jefw|deh|fred|rdmiller|ccbld01|japatel|baileyc|kure|dennisryan|sunkara|savant|adelmann|akhedkar|rsp1|ganbote|shivalkar|blondin|jhomer|montools|oellig|palrecha|team|ahuja|sstowell|davemac|latest|peacock|bld|karlo|kim|hkwalker|scottmcd|tkenny|nmeier|breight|clauson|befree|tjennings)_[^\/]*\//\//g;

		$f =~ s/^vobs\/(Softphone|iClarity|PoP|PWSP32|SCAPI32)\//IP_SoftPhone\//g;
		
		if ($f =~ /^(vob[1-7]|thinclient|systest)\//){
			$f = cleanIC ($f);
		}
		$f =~ s/^vobs\/bcmrd\//IC\//;


		if ($f =~ /^vobs\/ccs/){
			$f =~ s/^vobs\/(ccs|ccs_2test_comp1)\///;
			$f =~ s/^vobs\/ccs_test[0-9]_comp\///;
			$f = cleanCCS ($f);
		}
		
		if ($f =~ /^vobs\/(cstone|webclient|victest.pj|jcstest.pj|MsgCore|msgclients|intuity_[^\/]*)\//){
			$f = cleanIntuity ($f);
		}
		if ($f =~ /^vobs\/(aesvcs|aesvcs_rel|cpSharedSvcs|comp_aesvcs1|MVAPBase)\//){
			$f = cleanCmapi ($f);
		}

		if ($f =~ /^vobs\/cms.pj/){
			$f =~ s/\/(hgl|abram|baileyc|wilkison|scottmcd|yvette|rik|)_[^\/]+\//\//g;
			$f =~ s/\/(hgl|abram|baileyc|wilkison|scottmcd|yvette|rik|)_[^\/]+\//\//g;
			$f =~ s/\/(hgl|abram|baileyc|wilkison|scottmcd|yvette|rik|)_[^\/]+\//\//g;
			$f =~ s/\/(R1[0-9]AUXrc|ccinteg|r13.1aux|main)\//\//g;
			$f =~ s/\/(R1[0-9]AUXrc|ccinteg|r13.1aux|main)\//\//g;
			$f =~ s/\/(R1[0-9]AUXrc|ccinteg|r13.1aux|main)\//\//g;
			$f =~ s/^vobs\/cms.pj\//CMS\//;
		}
		
		if ($f =~ /^vobs\/(LAS|AS|GUI|Common|CM)\//){
			$f = cleanTenovis ($f);
		}
		
		if ($f =~ /^vobs\/(ivr_[^\/]*|ivr)\//){
			$f = cleanIvr ($f);
		}		

		if ($f =~ /^vobs\/v_[^\/]*\/|^v_[^\/]*\//){
			$f = cleanDialer ($f);
		}
		
		if ($f =~ /^vobs\/(victoryvs|geneva|UM[^\/]*)\//){
			$f = cleanMM($f);
		}
		
		if ($f =~ /^vobs\/\.wrs[^\/]*\//){
			$f = cleanWrs ($f);
		}
		$f =~ s/^vobs\/\.its[^\/]*\//ITS\//;
		$f =~ s/^vobs\/\.(hdrs|[^\/\.]+\.ss)*\//OLD\//;
		
		if ($f =~ /^vobs\/deftyfw\.pj|^vobs\/\.(tn[xbdp0-9]*|flw_ryon|media_gateway|hummer|boxster)\//){
			$f = cleanTns($f);
		}
		if ($f =~ /^vobs\/(cs_[^\/]*|viper|diamond2|coreservices|coreservices_rel)\//){
			$f = cleanCore($f);
		}
		
		if ($f =~ /^vobs\/smbs_[^\/]*\//){
			$f = cleanSmbs ($f);
		}
		if ($f =~ /^vobs\/ccr\//){
			$f = cleanCCR($f);
		}
		if ($f =~ /^vobs\/IPTel\//){
			$f = cleanIptel($f);
		}
		if ($f =~ /^vobs\/pt\.pj\//){
			$f = cleanPt ($f);
		}
		if ($f =~ /^vobs\/ic\.pj\//){
			$f = cleanIcpj ($f);
		}
		if ($f =~ /^vobs\/(eqm|cirs)\//){
			$f = cleanExpertnet ($f);
		}
			
		
		$f =~ s/^vobs\///;
	}
	#now deal with the rest
	my @x = split(/\@\@/, $f, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		$f = "$x[0]$x[1]";

		$f =~ s/\/(bld|TestBl|T221A|PUMA|UCI|PCD2|SANITYCHECKER|ADNWEB|WBM|DCP|TPCD20|T221M|PhnXChg[0-9]|ss72|team|brahme|sstowell|agitar|ajoshi|rrw|axis\-[1-9]|fisk|jpreed|minshengshen|shashisharma|riser|phone|PhoneXChange|bbw1|gchatterjee|yanxiaqu|matthewmartin|kasinadhuni|beedkar|parekh|arjitaghosh|mgeist|van|ack|gah1|yanxiaqu|Pkhanduri|abhisarw|kevindresher|rozensky|chg|main|msheng|cps|mrbrown|mchi|cclzvo|skudera|deh|rsmith9|smyrk|vklalka|rajinder|larsen|blondin|bessette|teveritt|ramuller|nijnam|nijnam|mikeg|davew|jrocca|masuma|phd|wannern|schmaus|ggf|icahoon|emila|cbao|tn[0-9]*|nam|angevine|rbartholomew|shengm|mmjohnson|barbwest|subhenduroy|hkwalker|katare|embedded|overlan|jstiles|jsloan|jmctee|dseaton|team|tah|rlm|adp|ewsup|bashir|jfhenry|mslinda|arankine|rdmiller|hrp|rowe|tminke|ieyeson|joele|nwp|nrb|un332b|baileyc|dsbrown3)_[^\/]+\//\//g;
		$f =~ s/\/(bld|TestBl|T221A|PUMA|UCI|PCD2|SANITYCHECKER|ADNWEB|WBM|DCP|TPCD20|T221M|PhnXChg[0-9]|ss72|team|brahme|sstowell|agitar|ajoshi|rrw|axis\-[1-9]|fisk|jpreed|minshengshen|shashisharma|riser|phone|PhoneXChange|bbw1|gchatterjee|yanxiaqu|matthewmartin|kasinadhuni|beedkar|parekh|arjitaghosh|mgeist|van|ack|gah1|yanxiaqu|Pkhanduri|abhisarw|kevindresher|rozensky|chg|main|msheng|cps|mrbrown|mchi|cclzvo|skudera|deh|rsmith9|smyrk|vklalka|rajinder|larsen|blondin|bessette|teveritt|ramuller|nijnam|nijnam|mikeg|davew|jrocca|masuma|phd|wannern|schmaus|ggf|icahoon|emila|cbao|tn[0-9]*|nam|angevine|rbartholomew|shengm|mmjohnson|barbwest|subhenduroy|hkwalker|katare|embedded|overlan|jstiles|jsloan|jmctee|dseaton|team|tah|rlm|adp|ewsup|bashir|jfhenry|mslinda|arankine|rdmiller|hrp|rowe|tminke|ieyeson|joele|nwp|nrb|un332b|baileyc|dsbrown3)_[^\/]+\//\//g;
		$f =~ s/\/(bld|TestBl|T221A|PUMA|UCI|PCD2|SANITYCHECKER|ADNWEB|WBM|DCP|TPCD20|T221M|PhnXChg[0-9]|ss72|team|brahme|sstowell|agitar|ajoshi|rrw|axis\-[1-9]|fisk|jpreed|minshengshen|shashisharma|riser|phone|PhoneXChange|bbw1|gchatterjee|yanxiaqu|matthewmartin|kasinadhuni|beedkar|parekh|arjitaghosh|mgeist|van|ack|gah1|yanxiaqu|Pkhanduri|abhisarw|kevindresher|rozensky|chg|main|msheng|cps|mrbrown|mchi|cclzvo|skudera|deh|rsmith9|smyrk|vklalka|rajinder|larsen|blondin|bessette|teveritt|ramuller|nijnam|nijnam|mikeg|davew|jrocca|masuma|phd|wannern|schmaus|ggf|icahoon|emila|cbao|tn[0-9]*|nam|angevine|rbartholomew|shengm|mmjohnson|barbwest|subhenduroy|hkwalker|katare|embedded|overlan|jstiles|jsloan|jmctee|dseaton|team|tah|rlm|adp|ewsup|bashir|jfhenry|mslinda|arankine|rdmiller|hrp|rowe|tminke|ieyeson|joele|nwp|nrb|un332b|baileyc|dsbrown3)_[^\/]+\//\//g;


		$f =~ s/\/(schmaus|mrs|omniORB|apache-ant|clover-ant|mule)-[^\/]+\//\//g;
		$f =~ s/\/(schmaus|mrs|omniORB|apache-ant|clover-ant|mule)-[^\/]+\//\//g;
		$f =~ s/\/(schmaus|mrs|omniORB|apache-ant|clover-ant|mule)-[^\/]+\//\//g;



		$f =~ s/\/(main|mrs-hw[0-9]*|aoa[0-9]+|root|CHECKEDOUT\.[0-9]+)\//\//g;
		$f =~ s/\/(main|mrs-hw[0-9]*|aoa[0-9]+|root|CHECKEDOUT\.[0-9]+)\//\//g;
		$f =~ s/\/(main|mrs-hw[0-9]*|aoa[0-9]+|root|CHECKEDOUT\.[0-9]+)\//\//g;

		$f =~ s/\/[^\/]+_([Dd][Ee][Vv]|[Ii]ntegration|fra|sbc|dev|nan|den|delivery[0-9]*|priv|SRTP|implementation)\//\//g;




		$f =~ s/\/[0-9][0-9][0-9]\//\//g;
		$f =~ s/\/[0-9][0-9]\//\//g;
		$f =~ s/\/[0-9][0-9]\//\//g;
		$f =~ s/\/[0-9]\//\//g;
		$f =~ s/\/[0-9]\//\//g;
		$f =~ s/\/\.\//\//;
		$f =~ s/\/\.0\//\//;
		$f =~ s/^\///;
	}
	
	(split(/\;/, $f, -1))[0];
}


sub cleanCCS {
	my $f = @_[0];
	#$f=~s/\/ccs[0-9].[0-9hirn]+\//\//g;
	#$f=~s/^ccs[0-9].[0-9hirn]+\///g;	
	$f=~s/^(impress21|oops)\///;
	$f=~s/\/(lss|ccs|ccs[hirn]|mvss|ses)[0-9]\.[0-9hirn]*\//\//g;
	$f=~s/^(asf|lss|ccs|ccs[hirn]|mvss|ses)[0-9]\.[0-9hirn]*\///g;
	$f =~ s/^\///;
	$f=~s/^(asf|lss|ccs|ccs[hirn]|mvss|ses)[0-9]\.[0-9hirn]*\.[0-9]\///;
	
	$f=~s/^(asf|lss|ccs|mvss|ses)\///;
	#$f=~s/^(lss|ccs|ccs[hirn]|mvss|ses)[0-9]\///;
	#


	$f=~s/\/ssf5\.5\.[0-9]_[^\/]+\//\//g;

	$f=~s/\/(sip|ssf5|ssf5\.5\.[0-9]|ssf7.0|ssf5|sip|es)_[^\/]+\//\//g;
	$f=~s/\/(sip|ssf5|ssf5\.5\.[0-9]|ssf7.0|ssf5|sip|es)_[^\/]+\//\//g;
	$f=~s/\/(sip|ssf5|ssf5\.5\.[0-9]|ssf7.0|ssf5|sip|es)_[^\/]+\//\//g;
	$f=~s/\/(sip|ssf5|ssf5\.5\.[0-9]|ssf7.0|ssf5|sip|es)_[^\/]+\//\//g;
	$f=~s/\/(cobertura-1.8|junit3.8.1)\//\//g;

	"CCS/$f";
}

sub cleanCmapi {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if (!($dir =~ /^vobs\/(aesvcs|aesvcs_rel|cpSharedSvcs|comp_aesvcs1|MVAPBase)\//)){
		return $d;
	}
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}


	$dir =~ s/\/(ODC|ODC2|mvcs|suzhen|mgreen|pboschert|peng|greway|gwilym|ikes|joele|narayanappa|myerly|mvivino|nawasthi|breight|bwen|kirkseader|bwen|chiiling|janak|peng|ikes|joele|mvivino|joelmack|core|janak|sgumbula|vklalka|ga|aes|apritche|hyoungjoolee|ronli|kim|atiwari|barbwest|dsthomas|neidetcher|ndronen|schmelmer|carda|mgood|clauson|gyeh|jmctee|clong|team|befree|llent|skjones|lpaicurich|kbjensen|ckrueger)_[^\/]+\//\//g;
	$dir =~ s/\/(ODC|ODC2|mvcs|suzhen|mgreen|pboschert|peng|greway|gwilym|ikes|joele|narayanappa|myerly|mvivino|nawasthi|breight|bwen|kirkseader|bwen|chiiling|janak|peng|ikes|joele|mvivino|joelmack|core|janak|sgumbula|vklalka|ga|aes|apritche|hyoungjoolee|ronli|kim|atiwari|barbwest|dsthomas|neidetcher|ndronen|schmelmer|carda|mgood|clauson|gyeh|jmctee|clong|team|befree|llent|skjones|lpaicurich|kbjensen|ckrueger)_[^\/]+\//\//g;
	$dir =~ s/\/(ODC|ODC2|mvcs|suzhen|mgreen|pboschert|peng|greway|gwilym|ikes|joele|narayanappa|myerly|mvivino|nawasthi|breight|bwen|kirkseader|bwen|chiiling|janak|peng|ikes|joele|mvivino|joelmack|core|janak|sgumbula|vklalka|ga|aes|apritche|hyoungjoolee|ronli|kim|atiwari|barbwest|dsthomas|neidetcher|ndronen|schmelmer|carda|mgood|clauson|gyeh|jmctee|clong|team|befree|llent|skjones|lpaicurich|kbjensen|ckrueger)_[^\/]+\//\//g;



	$dir =~ s/\/test-S[0-9]\//\/test\//g;
	$dir =~ s/\/Tserver[0-9]\//\/Tserver\//g;
	$dir =~ s/\/[Ll]inux[0-9]\//\/linux\//g;
	$dir =~ s/\/solaris[0-9]\//\/solaris\//g;
	$dir =~ s/\/[0-9].0.0.[0-9]\//\//g;
	$dir =~ s/\/CHECKEDOUT\.[0-9]+\//\//g;
	$dir =~ s/\/CHECKEDOUT\.[0-9]+\//\//g;
	$dir =~ s/\/CHECKEDOUT\.[0-9]+\//\//g;

	$dir =~ s/\/(LATEST|ccinteg|main|zp[0-9]+|root|CHECKEDOUT\.[0-9]+)\//\//g;
	$dir =~ s/\/(LATEST|ccinteg|main|zp[0-9]+|root|CHECKEDOUT\.[0-9]+)\//\//g;
	$dir =~ s/\/(LATEST|ccinteg|main|zp[0-9]+|root|CHECKEDOUT\.[0-9]+)\//\//g;
	$dir =~ s/\/(LATEST|ccinteg|main|zp[0-9]+|root|CHECKEDOUT\.[0-9]+)\//\//g;
	$dir =~ s/\/(LATEST|ccinteg|main|zp[0-9]+|root|CHECKEDOUT\.[0-9]+)\//\//g;

	$dir =~ s/\/[0-9][0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/\.\//\//;
	$dir =~ s/\/\.0\//\//;
	$dir =~ s/^\///;

	
	$dir =~ s/^vobs\/(aesvcs|aesvcs_rel|cpSharedSvcs|comp_aesvcs1|MVAPBase)\//CMAPI\//;
	$dir;
}
			 

sub cleanIC {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if (!($dir =~ /^(vob[1-7]|thinclient|systest)\//)){
		return $d;
	}
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}

	$dir =~ s/\/(5.6sp1|alexb5648|aicvcfs7|CentreVu CT For Siebel|06-Implementation|sea75x|ralphw|Jtel102|wc2\.4|OpCon00[0-9]|6\.1\.1|6\.0|6\.0I|7\.0_Files|7\.1|7\.xDocs|6\.xDocs|5\.xDocs|5\.1Docs|Tradewind|integrations|siebel|SAVE)\//\//g;
	$dir =~ s/\/(5.6sp1|alexb5648|aicvcfs7|CentreVu CT For Siebel|06-Implementation|sea75x|ralphw|Jtel102|wc2\.4|OpCon00[0-9]|6\.1\.1|6\.0|6\.0I|7\.0_Files|7\.1|7\.xDocs|6\.xDocs|5\.xDocs|5\.1Docs|Tradewind|integrations|siebel|SAVE)\//\//g;
		
	$dir =~ s/\/(rsmith9|IM[0-9]+|ct4s7|ic71|columbus|cq|dco|mttlogger|imc31|smyrk|nab|juggernaut|webcenter|aic71x|aic60xsiebelconnector|crmaa|wc2.4|siebel|adv|qes|ps|pi|wc|ct|cvuis|aic|aic60|econtact|centrevu|econtact56|aicvcfs7|source|tradewind|rohanl|martins|hcc|asifshaikh|mustangmmc)_[^\/]+\//\//g;
	$dir =~ s/\/(rsmith9|IM[0-9]+|ct4s7|ic71|columbus|cq|dco|mttlogger|imc31|smyrk|nab|juggernaut|webcenter|aic71x|aic60xsiebelconnector|crmaa|wc2.4|siebel|adv|qes|ps|pi|wc|ct|cvuis|aic|aic60|econtact|centrevu|econtact56|aicvcfs7|source|tradewind|rohanl|martins|hcc|asifshaikh|mustangmmc)_[^\/]+\//\//g;
	$dir =~ s/\/(rsmith9|IM[0-9]+|ct4s7|ic71|columbus|cq|dco|mttlogger|imc31|smyrk|nab|juggernaut|webcenter|aic71x|aic60xsiebelconnector|crmaa|wc2.4|siebel|adv|qes|ps|pi|wc|ct|cvuis|aic|aic60|econtact|centrevu|econtact56|aicvcfs7|source|tradewind|rohanl|martins|hcc|asifshaikh|mustangmmc)_[^\/]+\//\//g;


	
	$dir =~ s/\/main\//\//g;
	$dir =~ s/\/[0-9][0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/\.\//\//;
	$dir =~ s/\/\.0\//\//;
	$dir =~ s/^(vob[1-7]|thinclient|systest)\//IC\//;
	$dir;
}
	

sub cleanIvr {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if ($dir !~ /^vobs\/(ivr_[^\/]*|ivr)\//){
		return $d;
	}
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}

	$dir =~ s/\/(ivr[0-9]|ivrv[0-9])\//\//g;

	$dir =~ s/\/(sunport|jefft|lynnj|gupte|srl|emila|cti|lukek|tbct|lynnj|befree|bje|carlosne|llent|dmpark|ktam|helenaf|peng|bsunil|mkashelikar|ankur|hliu|elleng|van|mwick|ssunil|bolands|melarsen|sumesh|team|jpfax|spkwoss|vxml|gerhart|mwick|weh)_[^\/]+\//\//g;
	$dir =~ s/\/(sunport|jefft|lynnj|gupte|srl|emila|cti|lukek|tbct|lynnj|befree|bje|carlosne|llent|dmpark|ktam|helenaf|peng|bsunil|mkashelikar|ankur|hliu|elleng|van|mwick|ssunil|bolands|melarsen|sumesh|team|jpfax|spkwoss|vxml|gerhart|mwick|weh)_[^\/]+\//\//g;
	$dir =~ s/\/(sunport|jefft|lynnj|gupte|srl|emila|cti|lukek|tbct|lynnj|befree|bje|carlosne|llent|dmpark|ktam|helenaf|peng|bsunil|mkashelikar|ankur|hliu|elleng|van|mwick|ssunil|bolands|melarsen|sumesh|team|jpfax|spkwoss|vxml|gerhart|mwick|weh)_[^\/]+\//\//g;



	$dir =~ s/\/[^\/]+_(patch)\//\//g;

	$dir =~ s/\/main\//\//g;
	$dir =~ s/\/[0-9][0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/\.\//\//;
	$dir =~ s/^vobs\/(ivr_[^\/]*|ivr)\//IVR\//;
	$dir;
}
	
sub cleanTenovis {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if ($dir !~ /^vobs\/(LAS|AS|GUI|Common|CM)\//){
		return $d;
	}
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}

	$dir =~ s/\/(ASN.1|main)\//\//g;
	$dir =~ s/\/(ASN.1|main)\//\//g;
	$dir =~ s/\/(ASN.1|main)\//\//g;
	$dir =~ s/\/(ASN.1|main)\//\//g;

	$dir =~ s/\/(src|PUMA|WBM|ADNWEB)_[^\/]+\//\//g;
	$dir =~ s/\/(src|PUMA|WBM|ADNWEB)_[^\/]+\//\//g;
	$dir =~ s/\/(src|PUMA|WBM|ADNWEB)_[^\/]+\//\//g;
	$dir =~ s/\/(src|PUMA|WBM|ADNWEB)_[^\/]+\//\//g;
	$dir =~ s/\/(src|PUMA|WBM|ADNWEB)_[^\/]+\//\//g;

	$dir =~ s/\/[^\/]+_(fra|lvc|bgl|dev|den|nan|)\//\//g;
	$dir =~ s/\/[^\/]+_(fra|lvc|bgl|dev|den|nan|)\//\//g;
	$dir =~ s/\/[^\/]+_(fra|lvc|bgl|dev|den|nan|)\//\//g;
	$dir =~ s/\/[^\/]+_(fra|lvc|bgl|dev|den|nan|)\//\//g;


	$dir =~ s/\/[0-9][0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/\.\//\//;
	$dir =~ s/^vobs\/(LAS|AS|GUI|Common|CM)\//TENOVIS\//;
	$dir;
}
	
sub cleanDialer {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if ($dir !~ /^vobs\/v_[^\/]*\/|^v_[^\/]*\//){
		return $d;
	}
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}

	$dir =~ s/\/[0-9][0-9]-removed[_-][^\/]+\//\//g;
	$dir =~ s/\/[0-9][0-9]-removed[_-][^\/]+\//\//g;
	$dir =~ s/\/[0-9][0-9]-removed[_-][^\/]+\//\//g;

	$dir =~ s/\/[^\/]+ build\//\//g;
	$dir =~ s/\/[^\/]+ build\//\//g;
	$dir =~ s/\/[^\/]+ build\//\//g;
	
	$dir =~ s/\/(TAO|tao|sd6.0|agent_desktop|sd5.6|cd4.0|dev[0-9]|dev[0-9][0-9]|dev[0-9][0-9]cpp|CHECKEDOUT\.[0-9]+)\//\//g;
	$dir =~ s/\/(TAO|tao|sd6.0|agent_desktop|sd5.6|cd4.0|dev[0-9]|dev[0-9][0-9]|dev[0-9][0-9]cpp|CHECKEDOUT\.[0-9]+)\//\//g;

	$dir =~ s/\/(Bug|TAO|OA|OLD|windev|K24|sdn[0-9]+|oracle|MR|tk|columbus|connieb|kavithak|bhavin|pooja|hemant|davidw|markha|bridgetr|nuisance|cppunit|redwood|qiansu|tao|dev[0-9]+|xalan|xerces|aic|MBNA|jimh|qians|davidpa|qiansun|sergey)_[^\/]+\//\//g;
	$dir =~ s/\/(Bug|TAO|OA|OLD|windev|K24|sdn[0-9]+|oracle|MR|tk|columbus|connieb|kavithak|bhavin|pooja|hemant|davidw|markha|bridgetr|nuisance|cppunit|redwood|qiansu|tao|dev[0-9]+|xalan|xerces|aic|MBNA|jimh|qians|davidpa|qiansun|sergey)_[^\/]+\//\//g;
	$dir =~ s/\/(Bug|TAO|OA|OLD|windev|K24|sdn[0-9]+|oracle|MR|tk|columbus|connieb|kavithak|bhavin|pooja|hemant|davidw|markha|bridgetr|nuisance|cppunit|redwood|qiansu|tao|dev[0-9]+|xalan|xerces|aic|MBNA|jimh|qians|davidpa|qiansun|sergey)_[^\/]+\//\//g;



	$dir =~ s/\/[^\/]+_(sbc)\//\//g;
	$dir =~ s/\/[^\/]+_(sbc)\//\//g;
	$dir =~ s/\/[^\/]+_(sbc)\//\//g;

	$dir =~ s/\/main\//\//g;
	$dir =~ s/\/[0-9][0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/\.\//\//;
	$dir =~ s/^vobs\/v_[^\/]*\/|^v_[^\/]*\//DIALER\//;
	$dir;
}
	
sub cleanIcpj {
	#SIUS-IP Messaging T11B         : bbillinger@avaya.com
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if ($dir !~ /^vobs\/ic\.pj\//){
		return $d;
	}
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}
	$dir =~ s/main\///g;
	$dir =~ s/\/[0-9]+\//\//g;	
		
	$dir =~ s/\/(nc6.5)\//\//g;

	$dir =~ s/\/(team|ammerman|pingj|paulp|jli38|chaiwat|rad|janicejo|bli10|vlombart)_[^\/]*\//\//g;
	$dir =~ s/\/(team|ammerman|pingj|paulp|jli38|chaiwat|rad|janicejo|bli10|vlombart)_[^\/]*\//\//g;
	$dir =~ s/\/(team|ammerman|pingj|paulp|jli38|chaiwat|rad|janicejo|bli10|vlombart)_[^\/]*\//\//g;
	$dir =~ s/\/(team|ammerman|pingj|paulp|jli38|chaiwat|rad|janicejo|bli10|vlombart)_[^\/]*\//\//g;


	$dir =~ s/\/\.\//\//;
	$dir =~ s/\/NWcore\.pj\//\//;
	$dir =~ s/vobs\/ic\.pj\//intuity\//;
	$dir;
}

sub cleanIntuity {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if (!($dir =~ /^vobs\/(cstone|webclient|victest.pj|jcstest.pj|MsgCore|msgclients|intuity_[^\/]*)/)){
		return $d;
	}
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}
	$dir =~ s/\/([Cc][Ss][0-9\.rfu]*|[0-9.]*|IALX[0-9.]|POET|mss1.3-12rfu|main\/ialx)\//\//g;
	$dir =~ s/\/([Cc][Ss][0-9\.rfu]*|[0-9.]*|IALX[0-9.]|POET|mss1.3-12rfu|main\/ialx)\//\//g;
	$dir =~ s/\/([Cc][Ss][0-9\.rfu]*|[0-9.]*|IALX[0-9.]|POET|mss1.3-12rfu|main\/ialx)\//\//g;


	$dir =~ s/\/(dprenger|paulp|provo|dmoeller|nigro|jeff|pingj)_[^\/]+\//\//g;
	$dir =~ s/\/(dprenger|paulp|provo|dmoeller|nigro|jeff|pingj)_[^\/]+\//\//g;
	$dir =~ s/\/(dprenger|paulp|provo|dmoeller|nigro|jeff|pingj)_[^\/]+\//\//g;
	$dir =~ s/\/[^\/]+_(ctoolview)\//\//g;
	
	$dir =~ s/\/main\//\//g;
	$dir =~ s/\/[0-9][0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/\.\//\//;
	$dir =~ s/\/\.0\//\//;

	$dir =~ s/\/(MsgCore|NWcore)\.pj\//\//;
	$dir =~ s/^vobs\/(cstone|webclient|victest.pj|jcstest.pj|MsgCore|msgclients|intuity_[^\/]*)\//intuity\//;
	$dir;
}


sub cleanMM {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if ($dir !~ /^vobs\/(victoryvs|victory_play|geneva|UM[^\/]*)\/|^(UM|victory_play|geneva|tuiprompts|victoryvs|PlatformSDK|NotesSDK|WindowsInstaller|src|PQA|cirs|viper|vxml_play|voicexml)/){
		return $d;
	}
	
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}

	$dir =~ s/\/um[0-9].[0-9]*branch\//\//g;
	$dir =~ s/\/um[0-9].[0-9]*branch\//\//g;
	$dir =~ s/\/um[0-9].[0-9]*branch\//\//g;
	$dir =~ s/\/main\//\//g;

	$dir =~ s/\/(cirs|BAREBONES|netdiff|se|viper|cirr|POST|entity|VIPER|jamesp|darwin|patch1|Integration1.0|AMRTG|Target|v|jwirecki|agent|[0-9][0-9]|innsbruck|iptel|redmond|main|inverness|jupiter|UM|tk)_[^\/]+\//\//g;
	$dir =~ s/\/(cirs|BAREBONES|netdiff|se|viper|cirr|POST|entity|VIPER|jamesp|darwin|patch1|Integration1.0|AMRTG|Target|v|jwirecki|agent|[0-9][0-9]|innsbruck|iptel|redmond|main|inverness|jupiter|UM|tk)_[^\/]+\//\//g;
	$dir =~ s/\/(cirs|BAREBONES|netdiff|se|viper|cirr|POST|entity|VIPER|jamesp|darwin|patch1|Integration1.0|AMRTG|Target|v|jwirecki|agent|[0-9][0-9]|innsbruck|iptel|redmond|main|inverness|jupiter|UM|tk)_[^\/]+\//\//g;
	
	$dir =~ s/(Integration1.0|xerces-c-src1_7_0|html_hooks|upload_feature|usage_report|fop-0.20.4|geneva|mumbai|paris|innsbruck|inverness)\///g;
	
	
	$dir =~ s/\/[0-9][0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/\.\//\//;
	$dir =~ s/\/\.0\//\//;

	$dir =~ s/\/cs\.bk\//\/cs\//;
	$dir =~ s/^vobs\///;	

	"MM/$dir";
}
sub cleanWrs {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if (!($dir =~ /^vobs\/\.wrs[^\/]*\//)){
		return $d;
	}
	$dir =~ s/\/\.\@\@\//\//;
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}

	
	$dir =~ s/\/ipsi_ess_b\//\//g;
	$dir =~ s/\/ipsi_ess_b\//\//g;
	$dir =~ s/\/ipsi_ess_b\//\//g;

	$dir =~ s/\/(mrs)-[^\/]+\//\//g;


	$dir =~ s/\/(tn[xbdpac0-9]*|cyf|tran|wgb|gggbelljhemstre||merklin|seaton|wgb|cl_[0-9]+|cld_[0-9]+|lmm2_[0-9]+|ps57|sr31|ygoldber|lveksler|alexrios|aroran|stiles|imad|praj|rsharma|ars|tod|dmacphee|ggustafs|yeager|dholland|rjheck|bllarson|jhomer|fisk|ipsi_v3|awilton|dsholland|komer|sgp|matamny|jmctee|jewalt|saad|thavisri|ribl|liz|mraj|jmctee|mslinda|crduncan|dseaton|ras|tjn|ldg|jobany|fattore|mattc|homer|mslinda|anthony|glh|tminke|ljr|sxiong1|kneilson|jsloan|jkho|gwilym|thorsten|egp|zhihu|team|dfl|cbwood|sasson|embedded|madden|nmoneta|ysabbah|cliff|aerez|smimmi|ngurfink|overlan)_[^\/]+\//\//g;
	$dir =~ s/\/(tn[xbdpac0-9]*|cyf|tran|wgb|gggbelljhemstre||merklin|seaton|wgb|cl_[0-9]+|cld_[0-9]+|lmm2_[0-9]+|ps57|sr31|ygoldber|lveksler|alexrios|aroran|stiles|imad|praj|rsharma|ars|tod|dmacphee|ggustafs|yeager|dholland|rjheck|bllarson|jhomer|fisk|ipsi_v3|awilton|dsholland|komer|sgp|matamny|jmctee|jewalt|saad|thavisri|ribl|liz|mraj|jmctee|mslinda|crduncan|dseaton|ras|tjn|ldg|jobany|fattore|mattc|homer|mslinda|anthony|glh|tminke|ljr|sxiong1|kneilson|jsloan|jkho|gwilym|thorsten|egp|zhihu|team|dfl|cbwood|sasson|embedded|madden|nmoneta|ysabbah|cliff|aerez|smimmi|ngurfink|overlan)_[^\/]+\//\//g;
	$dir =~ s/\/(tn[xbdpac0-9]*|cyf|tran|wgb|gggbelljhemstre||merklin|seaton|wgb|cl_[0-9]+|cld_[0-9]+|lmm2_[0-9]+|ps57|sr31|ygoldber|lveksler|alexrios|aroran|stiles|imad|praj|rsharma|ars|tod|dmacphee|ggustafs|yeager|dholland|rjheck|bllarson|jhomer|fisk|ipsi_v3|awilton|dsholland|komer|sgp|matamny|jmctee|jewalt|saad|thavisri|ribl|liz|mraj|jmctee|mslinda|crduncan|dseaton|ras|tjn|ldg|jobany|fattore|mattc|homer|mslinda|anthony|glh|tminke|ljr|sxiong1|kneilson|jsloan|jkho|gwilym|thorsten|egp|zhihu|team|dfl|cbwood|sasson|embedded|madden|nmoneta|ysabbah|cliff|aerez|smimmi|ngurfink|overlan)_[^\/]+\//\//g;

	

	$dir =~ s/\/(src[0-9])\//\/src\//g;
	$dir =~ s/\/(arch[0-9])\//\/arc\//g;
	$dir =~ s/\/(main|target|wrs2|wrs2.2-ppc|mrs-hw[0-9]+)\//\//g;
	$dir =~ s/\/(main|target|wrs2|wrs2.2-ppc|mrs-hw[0-9]+)\//\//g;
	$dir =~ s/\/(main|target|wrs2|wrs2.2-ppc|mrs-hw[0-9]+)\//\//g;


	$dir =~ s/\/[0-9][0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;

	$dir =~ s/^vobs\/\.wrs[^\/]*\//TNS\//;
	
	$dir;
}	

sub cleanTns {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if (!($dir =~ /^vobs\/deftyfw\.pj|^vobs\/\.(tn[xbdp0-9]*|flw_ryon|media_gateway|hummer|boxster)\//)){		
		return $d;
	}
	$dir =~ s/\/\.\@\@\//\//;
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}

	$dir =~ s/\/(tn[xbdp0-9]*|cjn|rwb|kdg|holly|rdmiller|jhemstre|rg12|mkj|ccadmin|sclay|sec|seaton|agere|joele|wgb|ieyeson|wgb|dhinds|merklin|cl_[0-9]+|cld_[0-9]+|lmm2_[0-9]+|ps57|sr31|ygoldber|lveksler|alexrios|aroran|stiles|imad|praj|rsharma|ars|tod|dmacphee|ggustafs|yeager|dholland|rjheck|bllarson|jhomer|fisk|ipsi_v3|awilton|dsholland|komer|sgp|matamny|jmctee|jewalt|saad|thavisri|ribl|liz|mraj|jmctee|mslinda|crduncan|dseaton|ras|tjn|ldg|jobany|fattore|mattc|homer|mslinda|anthony|glh|tminke|ljr|sxiong1|kneilson|jsloan|jkho|gwilym|thorsten|egp|zhihu|team|dfl|cbwood|sasson|embedded|madden|nmoneta|ysabbah|cliff|aerez|smimmi|ngurfink|overlan)_[^\/]+\//\//g;
	$dir =~ s/\/(tn[xbdp0-9]*|cjn|rwb|kdg|holly|rdmiller|jhemstre|rg12|mkj|ccadmin|sclay|sec|seaton|agere|joele|wgb|ieyeson|wgb|dhinds|merklin|cl_[0-9]+|cld_[0-9]+|lmm2_[0-9]+|ps57|sr31|ygoldber|lveksler|alexrios|aroran|stiles|imad|praj|rsharma|ars|tod|dmacphee|ggustafs|yeager|dholland|rjheck|bllarson|jhomer|fisk|ipsi_v3|awilton|dsholland|komer|sgp|matamny|jmctee|jewalt|saad|thavisri|ribl|liz|mraj|jmctee|mslinda|crduncan|dseaton|ras|tjn|ldg|jobany|fattore|mattc|homer|mslinda|anthony|glh|tminke|ljr|sxiong1|kneilson|jsloan|jkho|gwilym|thorsten|egp|zhihu|team|dfl|cbwood|sasson|embedded|madden|nmoneta|ysabbah|cliff|aerez|smimmi|ngurfink|overlan)_[^\/]+\//\//g;
	$dir =~ s/\/(tn[xbdp0-9]*|cjn|rwb|kdg|holly|rdmiller|jhemstre|rg12|mkj|ccadmin|sclay|sec|seaton|agere|joele|wgb|ieyeson|wgb|dhinds|merklin|cl_[0-9]+|cld_[0-9]+|lmm2_[0-9]+|ps57|sr31|ygoldber|lveksler|alexrios|aroran|stiles|imad|praj|rsharma|ars|tod|dmacphee|ggustafs|yeager|dholland|rjheck|bllarson|jhomer|fisk|ipsi_v3|awilton|dsholland|komer|sgp|matamny|jmctee|jewalt|saad|thavisri|ribl|liz|mraj|jmctee|mslinda|crduncan|dseaton|ras|tjn|ldg|jobany|fattore|mattc|homer|mslinda|anthony|glh|tminke|ljr|sxiong1|kneilson|jsloan|jkho|gwilym|thorsten|egp|zhihu|team|dfl|cbwood|sasson|embedded|madden|nmoneta|ysabbah|cliff|aerez|smimmi|ngurfink|overlan)_[^\/]+\//\//g;


	$dir =~ s/\/ipsi_ess_b\//\//g;
	$dir =~ s/\/ipsi_ess_b\//\//g;
	$dir =~ s/\/ipsi_ess_b\//\//g;

	$dir =~ s/\/(main)\//\//g;
	$dir =~ s/\/[0-9][0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;

	$dir =~ s/^vobs\/\.(tn[xbdp0-9]*|boxster|media_gateway|flw_ryon)\/(tn[xbdp0-9]*|boxster|media_gateway|flw_ryon)\.brd\//TNS\//;
	$dir =~ s/^vobs\/\.tn[xbdp0-9]*\//TNS\//;
	$dir =~ s/^vobs\/\.hummer\/hummer_gw\.brd\//TNS\//;
	$dir =~ s/^vobs\/\.boxster\/boxster\.brd\//TNS\//;
	$dir =~ s/^vobs\/deftyfw\.pj\//TNS\//;
	
	$dir;
}



sub cleanCore {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if ($dir !~ /^vobs\/(cs_[^\/]*|viper|diamond2|coreservices_rel|coreservices)\//){
		return $d;
	}

	$dir =~ s/\/\.\@\@\//\//;
	my $dd = $dir;
	#$dd =~ s/\@\@/\@/g;
	my @x = split(/\@\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		#$dir = "$x[0]/$f[$#f]";
		$dir = "$x[0]$x[1]";
		#print  "++ $dd\n";
		#print "+- $x[1] -- $x[2] -- $f[$#f]\n";
		#print  "-- $dir\n";
	}

	$dir =~ s/\/3\.[0-9]\.x\//\//g;
	$dir =~ s/\/3\.[0-9]\.x\//\//g;
	$dir =~ s/\/3\.[0-9]\.x\//\//g;


		
	
	$dir =~ s/\/(WebLM|BAREBONES|DCP|3\.1\.1|3\.[0-9]|sunnyjain|shivalkar|reddykalluri|ack|blondin|clauson|cassie|dennisryan|draginich|klages|kherrmann|pgaikwad|lakshmis|psrivastava|lmadhurao|mmehta|nmeier|palkonda|rajinder|sb24|team|bharvirkar|arjitaghosh|ajoshi|ppatadia|rodneyblack|savant|ak|atiwari|aul|dfl|divya|gyang|gyeh|hgulati|huixiawang|joelmack|kirkseader|krobert|llent|carda|dora|griesslr|sgumbula|pcherukuri|src|v|atf|bv|brahme|katare|teveritt|mgreen|lpaicurich|mschoenfelder|darwin|netdiff|sanh|sajja|smeena|snowdon|tfeng|vklalka|himawan|smadhey|barbwest|xin|VIPER|v_js|v_tcd|v_tcd2|dherrick|clong)_[^\/]+\//\//g;
	$dir =~ s/\/(WebLM|BAREBONES|DCP|3\.1\.1|3\.[0-9]|ack|blondin|clauson|cassie|dennisryan|draginich|klages|lakshmis|lmadhurao|mmehta|nmeier|palkonda|rajinder|sb24|team|bharvirkar|arjitaghosh|ajoshi|ppatadia|rodneyblack|savant|ak|atiwari|aul|dfl|divya|gyang|gyeh|hgulati|huixiawang|joelmack|kirkseader|krobert|llent|carda|dora|griesslr|sgumbula|pcherukuri|src|v|atf|bv|brahme|katare|teveritt|mgreen|lpaicurich|mschoenfelder|darwin|netdiff|sanh|sajja|smeena|snowdon|tfeng|vklalka|himawan|smadhey|barbwest|xin|VIPER|v_js|v_tcd|v_tcd2|dherrick|clong)_[^\/]+\//\//g;
	$dir =~ s/\/(WebLM|BAREBONES|DCP|3\.1\.1|3\.[0-9]|ack|blondin|clauson|cassie|dennisryan|draginich|klages|lakshmis|lmadhurao|mmehta|nmeier|palkonda|rajinder|sb24|team|bharvirkar|arjitaghosh|ajoshi|ppatadia|rodneyblack|savant|ak|atiwari|aul|dfl|divya|gyang|gyeh|hgulati|huixiawang|joelmack|kirkseader|krobert|llent|carda|dora|griesslr|sgumbula|pcherukuri|src|v|atf|bv|brahme|katare|teveritt|mgreen|lpaicurich|mschoenfelder|darwin|netdiff|sanh|sajja|smeena|snowdon|tfeng|vklalka|himawan|smadhey|barbwest|xin|VIPER|v_js|v_tcd|v_tcd2|dherrick|clong)_[^\/]+\//\//g;


	
	$dir =~ s/\/[^\/]+_(config|install|product|pconfig)_[^\/]+\//\//g;
	$dir =~ s/\/[^\/]+_(config|install|product|pconfig)_[^\/]+\//\//g;
	$dir =~ s/\/[^\/]+_(config|install|product|pconfig)_[^\/]+\//\//g;
	$dir =~ s/\/[^\/]+_(config|install|product|pconfig)_[^\/]+\//\//g;


	$dir =~ s/^vobs\/(cs_[^\/]*|viper|diamond2|coreservices_rel|coreservices)\//CORESERVICES\//;
	$dir =~ s/CORESERVICES\/release\/cs_[^\/]*\//CORESERVICES\//;
	$dir =~ s/\/(drop|main|ccinteg|dublin|jsloan|jmstps-dev|sandbox|jmstps-1.0)\//\//g;
	$dir =~ s/\/(drop|main|ccinteg|dublin|jsloan|jmstps-dev|sandbox|jmstps-1.0)\//\//g;
	$dir =~ s/\/(drop|main|ccinteg|dublin|jsloan|jmstps-dev|sandbox|jmstps-1.0)\//\//g;
	$dir =~ s/\/3\.0.[0-9x]\//\//g;
	$dir =~ s/\/3\.0.[0-9x]\//\//g;

	$dir =~ s/\/[0-9][0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	return $dir;
}

	
sub cleanCCR {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if ($dir !~ /^vobs\/ccr\//){
		return $d;
	}
	$dir =~ s/\/.\@\@\//\//;$dir =~ s/\@\@/\;/;
	$dir =~ s/\/main\//\//g;
	$dir =~ s/\/ccinteg\//\//g;
	$dir =~ s/\/ccinteg\//\//g;
	$dir =~ s/\/team\//\//g;
	$dir =~ s/\/CCR\-[^\/]+\//\//g;
	$dir =~ s/\/CCR\-[^\/]+\//\//g;
	$dir =~ s/\/CCR\-[^\/]+\//\//g;
	$dir =~ s/\/CCR\-[^\/]+\//\//g;
	$dir =~ s/\/(CCR|jboss)\-[^\/]+\//\//g;
	
	$dir =~ s/\/[0-9]+\//\//g;
	$dir =~ s/\/[^\/]+_(ccr)_[^\/]+\//\//g;
	$dir =~ s/\/[^\/]+_(ccr)_[^\/]+\//\//g;
	$dir =~ s/\/[^\/]+_(ccr)_[^\/]+\//\//g;
	$dir =~ s/\/[^\/]+_(ccr)_[^\/]+\//\//g;

	$dir =~ s/\/ccr_[^\/]+\//\//g;
	$dir =~ s/\/ccr_[^\/]+\//\//g;
	$dir =~ s/\/ccr_[^\/]+\//\//g;

	$dir =~ s/\/(CCR|arjitaghosh|emilio|dsbrown3|whipple|zanoni|jbchen|tjennings|brahme|stevejacobson|ckrueger|bv|scottmcd|russellpierce|rhanagan|jhogan|llent|loots|hongguang|arthurs|bashir|cassie|breight|mdb|chu|ptendick|canoe|chiiling|sholt|helenafang|mgeist|khackbarth|xuanli|askhan|lajones|tommurray)_[^\/]+\//\//g;
	$dir =~ s/\/(CCR|arjitaghosh|emilio|dsbrown3|whipple|zanoni|jbchen|tjennings|brahme|stevejacobson|ckrueger|bv|scottmcd|russellpierce|rhanagan|jhogan|llent|loots|hongguang|arthurs|bashir|cassie|breight|mdb|chu|ptendick|canoe|chiiling|sholt|helenafang|mgeist|khackbarth|xuanli|askhan|lajones|tommurray)_[^\/]+\//\//g;
	$dir =~ s/\/(CCR|arjitaghosh|emilio|dsbrown3|whipple|zanoni|jbchen|tjennings|brahme|stevejacobson|ckrueger|bv|scottmcd|russellpierce|rhanagan|jhogan|llent|loots|hongguang|arthurs|bashir|cassie|breight|mdb|chu|ptendick|canoe|chiiling|sholt|helenafang|mgeist|khackbarth|xuanli|askhan|lajones|tommurray)_[^\/]+\//\//g;


	$dir =~ s/^\/vobs\/ccr\//CCR\//;
	$dir;
}
	
	
sub cleanIptel {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if ($dir !~ /^vobs\/IPTel\//){
		return $d;
	}
	$dir =~ s/\/.\@\@\//\//;$dir =~ s/\@\@/\;/;
	$dir =~ s/\/main\//\//g;
	$dir =~ s/\/[^\/]+_(Sablime|Sab|SIP2|SPC1|IPT[23])_[^\/]+\//\//g;
	$dir =~ s/\/[^\/]+_(Sablime|Sab|SIP2|SPC1|IPT[23])_[^\/]+\//\//g;
	$dir =~ s/\/[^\/]+_(Sablime|Integration)\//\//g;
	$dir =~ s/\/(tokarchuk|kevindresher|arana|SAS|yanxiaqu|yujunyang|dadani|vlee|mcrandall|gaugustino|nadia|mmurthy|epamo01|Sablime|skudera|matthewmartin|nkober|reisert|gah1|ressalei|jkuhar|jgill|mlundberg|garstin|beedkarnew)_[^\/]+\//\//g;
	$dir =~ s/\/(tokarchuk|kevindresher|arana|SAS|yanxiaqu|yujunyang|dadani|vlee|mcrandall|gaugustino|nadia|mmurthy|epamo01|Sablime|skudera|matthewmartin|nkober|reisert|gah1|ressalei|jkuhar|jgill|mlundberg|garstin|beedkarnew)_[^\/]+\//\//g;
	$dir =~ s/\/[0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9]\//\//g;
	$dir =~ s/\/[0-9][0-9][0-9]\//\//g;
	$dir =~ s/\/[^\/]+_(Sablime|Integration)\//\//g;
	$dir =~ s/\/[^\/]+_(Sablime|Integration)\//\//g;

	$dir =~ s/^vobs\/IPTel\//IPT\//;
	$dir;
}	
	
sub cleanSmbs {
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if ($dir !~ /^vobs\/smbs_[^\/]*\//){
		return $d;
	}else{
		$dir =~ s/\@\@\//\//;
		$dir =~ s/\@\@/\;/;
		
		$dir =~ s/main\///g;
		$dir =~ s/\/[0-9]+\//\//g;	
		
		$dir =~ s/\/(V3p2_[^\/]+|[^\/]+_V4p0|V4p0_[^\/]+|[^\/]+_V4p0_[^\/]+)\//\//g;
		$dir =~ s/\/(V3p2_[^\/]+|[^\/]+_V4p0|V4p0_[^\/]+|[^\/]+_V4p0_[^\/]+)\//\//g;

		$dir =~ s/\/(alex|ams|dave|dave_bugfix|devs_jmoore_0|dnh_release_dev|econf|emil|emil_[^\/]+|fhmeyer_config_service|frankfurt|gf_sv1|HG_Extns|jimd|jimd4|Keynote|lcmh|lcr|macfarlane_[^\/]+|makis_delic|remote_dial|rev_call||Samba|Ramzi[0-9]*|ph_release|raph|raph_work|recording|rev_call|sava_b[0-9]|sava_b_atm|sdh_pots8|sdw_vcm|stuart)\//\//g;

		$dir =~ s/\/(ViridianSamba|hardware_b|scn|scnHGs|sdw_tivcm|SEC|shaslam|ssa_frankfurt|ssa_frankfurt_p4|ssa_lincroft|ssa_lincroft_p4|ssa_lincroft_p4b|tango_jan|Winsock|Winsock_stable|tango_proj_0_Integration|tangowinsvr_Development|Twinning|unicode|V4_Makis|koutaiba|version4p0_T3IP|zxivanovich|zxivanovich_emc|AdvAnn|binzhang_sv1|Winsock_stable)\//\//g;	

		$dir =~ s/\/[^\/]+_(xhu|dallison|dave|shelford|smackintosh)_[^\/]+\//\//g;
		$dir =~ s/\/[^\/]*(yogeshkumar|shelford|pfulop|lafferty|scantlebury|acreed|smackintosh|dallison|richards)[^\/]*\//\//g;
		$dir =~ s/\/V_3p2\//\//g;

		$dir =~ s/\/(LZ|FFMY|smackintosh|devs|kumar|xhu|sdh|pki|zahmed|thaughton|tims|lafferty|richards)_[^\/]+\//\//g;		
		$dir =~ s/\/(LZ|FFMY|smackintosh|devs|kumar|xhu|sdh|pki|zahmed|thaughton|tims|lafferty|richards)_[^\/]+\//\//g;		
		$dir =~ s/\/(LZ|FFMY|smackintosh|devs|kumar|xhu|sdh|pki|zahmed|thaughton|tims|lafferty|richards)_[^\/]+\//\//g;		

		$dir =~ s/\/(TWS_Dev|tangowinsvr_Development|tango_jan|tango_proj_0_Integration|xhu|ykumar|zahmed)\//\//g;


		$dir =~ s/ConfigDB\/ConfigDB\//ConfigDB\//g;
		$dir =~ s/ConfigDB\/ConfigDB\//ConfigDB\//g;
		$dir =~ s/vobs\/smbs_V3p2\/IPO_3p2\//IPOffice\//;
		$dir =~ s/vobs\/smbs_code\/IPO_Dev\//IPOffice\//;
		$dir =~ s/vobs\/smbs_tango_com\/(tango|snode|home|demos|alsip)\//Tango\//;
	}
	$dir;
}
sub cleanPt {
	#some CM testing tool I presume: dehowell@avaya.com
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if ($dir !~ /^vobs\/pt\.pj\//){
		return $d;
	}
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}
	$dir =~ s/main\///g;
	$dir =~ s/\/[0-9]+\//\//g;	
		
	$dir =~ s/\/(team|deh|ua|rlm|sip)_[^\/]*\//\//g;
	$dir =~ s/\/(team|deh|ua|rlm|sip)_[^\/]*\//\//g;
	$dir =~ s/\/(team|deh|ua|rlm|sip)_[^\/]*\//\//g;
	$dir =~ s/\/(team|deh|ua|rlm|sip)_[^\/]*\//\//g;

	$dir =~ s/\/\.\//\//;
	$dir =~ s/vobs\/pt\.pj\//pt\//;
	$dir;
}

sub cleanExpertnet {
	#SIUS-GMS Ops-Cross LOB De       : Reha_Gur
	my $d = @_[0];
	$dir = $d;
	$dir =~ s/^\///;
	if ($dir !~ /^vobs\/(eqm|cirs)\//){
		return $d;
	}
	my $dd = $dir;
	$dd =~ s/\@\@/\@/g;
	my @x = split(/\@/, $dd, -1);
	if (scalar(@x) > 1){
		my @f = split (/\//, $x[1], -1);
		#this is not perfect as it removes directories after @
		$dir = "$x[0]$x[1]";
	}
	$dir =~ s/main\///g;
	$dir =~ s/\/[0-9]+\//\//g;	
		
	$dir =~ s/\/(nc6.5|impl|test|dao)\//\//g;
	$dir =~ s/\/(nc6.5|impl|test|dao)\//\//g;
	$dir =~ s/\/(nc6.5|impl|test|dao)\//\//g;

	$dir =~ s/\/[^\/]*_(exnet[0-9]+)\//\//g;
	$dir =~ s/\/[^\/]*_(exnet[0-9]+)_[^\/]*\//\//g;
	$dir =~ s/\/[^\/]*_(exnet[0-9]+)\//\//g;
	$dir =~ s/\/[^\/]*_(exnet[0-9]+)_[^\/]*\//\//g;
	$dir =~ s/\/[^\/]*_(exnet[0-9]+)\//\//g;
	$dir =~ s/\/[^\/]*_(exnet[0-9]+)_[^\/]*\//\//g;
	
	$dir =~ s/\/(EQM|AMRTG|ELAT|PVAT|agent|rohanl|team|hahluwalia|nhepworth|brao|gur|rlenard|jwirecki|tbardzil|snmp)_[^\/]*\//\//g;
	$dir =~ s/\/(EQM|AMRTG|ELAT|PVAT|agent|rohanl|team|hahluwalia|nhepworth|brao|gur|rlenard|jwirecki|tbardzil|snmp)_[^\/]*\//\//g;
	$dir =~ s/\/(EQM|AMRTG|ELAT|PVAT|agent|rohanl|team|hahluwalia|nhepworth|brao|gur|rlenard|jwirecki|tbardzil|snmp)_[^\/]*\//\//g;



	#for cirs
	$dir =~ s/\/cirs[0-9]+\//\//g;
	$dir =~ s/\/[0-9]+_[^\/]+\//\//g;
	$dir =~ s/\/[0-9]+_[^\/]+\//\//g;
	$dir =~ s/\/[0-9]+_[^\/]+\//\//g;
	$dir =~ s/\/[0-9]+_[^\/]+\//\//g;

	
	$dir =~ s/\/\.\//\//;
	$dir =~ s/vobs\/(eqm|cirs)\//eqm\//;
	$dir;
}

sub classifyLine {
	my ($line) = @_[0];
	my ($res)="none";
	$res="statement" if $line =~ /\;/;

	$res="proto" if $line =~ /\bvoid\b|\bint\b|\bshort\b|\blong\b|\bfloat\b|\bdouble\b|\bpublic\b|\bprivate\b|\bprotected\b|\bPROTO\b|\bstatic\b|\bboolean\b|\bunsigned\b|\bchar\b|\bextern\b|\bstruct\b|\bregister\b|\btypedef\b|\bunion\b/;
	$res="proto" if  $line =~ /\}\s*[A-z]+/;
	
	$res="call" if $line =~ /[A-z0-9_]+\s*\(/;

	$res="ctrl" if $line =~ /\bif\b|\belse\b|\belsif\b|\bwhile\b|\bswitch\b|\bdo\b|\bwhile\b|\buntil\b|\bbreak\b|\breturn\b|\bcontinue\b|\bdefault\b|\bcase\b|\bgoto\b/;
	
	$res="macro" if $line =~ /^\s*\#/;
	$res="comment" if $line =~ /^\s*\*|^\s*\/\*|^\s*\/\//;
	$res="blank" if $line =~ /^\s*$/;

	$res = "args" if ($line =~ /,/ && $res eq "none");
	$res = "block" if ($line =~ /[\{\}]/ && $res eq "none");
	$res = "statement" if ($line =~ /[^=]=[^=]/ && $res eq "none");
	$res;
}


1;
