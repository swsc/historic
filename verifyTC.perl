#!/usr/bin/perl
use Error qw(:try);
use strict;
use TokyoCabinet;
use Compress::LZF;
use Digest::MD5 qw(md5 md5_hex md5_base64);
my $debug = 0;
$debug = 1 if $ARGV[2] ne "";

my $fname="$ARGV[0]";
my (%clones);
if(!tie(%clones, "TokyoCabinet::HDB", "$fname",
		  TokyoCabinet::HDB::OREADER)){
	print STDERR "Error: tie for $fname\n";
}
my (%dat, %idat);
my $offset = 0;
sub safeDecomp {
	my ($codeC, $comm) = @_;
	try {
		my $code = "";
		$code = decompress ($codeC) if $codeC ne "";
		return $code;
   } catch Error with {
		my $ex = shift;
		print STDERR "Error\;$comm\; $ex\n";
		return "";
	}
}
while (my ($codeC, $vs) = each %clones){
	my $lC = length ($codeC);
	#print STDERR "$offset\;$vs\;".(length ($codeC))."\n" if $debug;
	my $code = safeDecomp ($codeC, "$vs:$offset:$lC");	
	my $l = length($code);
	#next if $l == 0;
	my ($dHC, $d64C, $dH, $d64) = (md5_hex($codeC), md5_base64($codeC), md5_hex($code), md5_base64($code));
	my $val = "$l\;$dHC\;$d64C\;$dH\;$d64";
	print "tch\;$offset\;$vs\;$lC\;$val\n" if $debug;
	$dat{$val}{$vs}++;
	$idat{$vs}{$val}++;
	$offset++;
}
untie %clones;

print STDERR "Read $offset\n";

use DB_File;
my $b = new DB_File::HASHINFO;
$b ->{cachesize}=1000000000;
$b ->{nelem} = 100000;
$b ->{bsize} = 4096;
$fname="$ARGV[1]";
tie %clones, "DB_File", $fname, O_RDONLY, 0666, $b
	or die "cant open file  $fname\n";

my $offset = 0;
while (my ($codeC, $vs) = each %clones){
	my $lC = length ($codeC);
	#print STDERR "$offset\;$vs\;".(length ($codeC))."\n" if $debug;
	my $code = safeDecomp ($codeC, "$vs:$offset:$lC");
	my $l = length($code);
	#next if $l == 0;
	my ($dHC, $d64C, $dH, $d64) = (md5_hex($codeC), md5_base64($codeC), md5_hex($code), md5_base64($code));
	my $val = "$l\;$dHC\;$d64C\;$dH\;$d64";
	print "db\;$offset\;$vs\;$lC\;$val\n" if $debug;
	if (! exists $dat{$val} || ! exists $dat{$val}{$vs}){
		print STDERR "a\;$offset\;$val\;$vs\n";
	}
	if (! exists $idat{$vs} || ! exists $idat{$vs}{$val}){
		print STDERR "b\;$offset\;$vs\;$val";
		if (exists $idat{$vs}){
			for my $vv (keys %{ $idat{$vs} }){
				print STDERR "\;$vv";
			}
		}
		print STDERR "\n";
	}
	$offset++;
}
untie %clones;
