use strict;
use warnings;
use Compress::LZF;

use line;
#########################
# create code to versions database
#########################
use TokyoCabinet;
sub enc {
        my $str = $_[0];
        $str =~ s/\[/\%5B/g;
        $str =~ s/\]/\%5D/g;
        $str =~ s/\#/\%23/g;
        $str;
}

my $fbase = $ARGV[0];
my $fname="$fbase.bin";
open F, ">$fname";

my $idx = 0;
open IDX, ">$fbase.idx";

my %cmds;
while(<STDIN>){
	chop();
	my $cmd = $_;
	my $path = $cmd;
	$path =~ s/^git \--git-dir\=\'//;
	$path =~ s/^([^\']*)\' show \'(.*)\'$/$2/;
	next if $path =~ /\=\>/;#indicates copies/renames:ccbd73a450bd33362373a3a066beffb348f528a7:lib/Test/Simple/t/{ => Builder}/output.t
	my $dir = $1;
	$dir =~ s/^git\///;
	$dir =~ s/\/\.git$//;
	$path =~ s/(.*):/$dir\//;
	my $vs = $1;
	if ($path =~ /\'/){
		$cmd =~ s/show '/show "/;
		$cmd =~ s/'$/"/;
	}
	if ($path ne ""){
		next if !($path =~ /$matchExt/);
		my $line = "";
		if (open CONT, "$cmd |"){
			while(<CONT>){
				$line .= $_;
			}
		}
		my $len = length($line);
		next if $len == 0;
		my $lineC = compress ($line);
		my $lenC = length($lineC);
		print IDX "$idx\;$lenC\;$path/$vs\n";
		print F $lineC;
		$idx+=$lenC;
	}
}
