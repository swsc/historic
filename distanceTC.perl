use TokyoCabinet;
use strict;
use warnings;
use Compress::LZF;
use Text::LevenshteinXS qw(distance);

sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}


my $hdb = TokyoCabinet::HDB->new();
if (!$hdb->open("$ARGV[0]", $hdb->OREADER)){
	my $ecode = $hdb->ecode();
   printf STDERR ("open error: %s\n", $hdb->errmsg($ecode));
}

my @vals = ();

$hdb ->iterinit();
while(defined(my $key = $hdb->iternext())){
	my $code = safeDecomp ($key);
	push @vals, $code;
}

for my $i (0..($#vals-1)){
	my $a = $vals[$i];
	for my $j (($i+1)..$#vals){
		my $b = $vals[$j];
		my $dist = TokyoCabinet::strdistance(\$a, \$b, 0);
		my $dist1 = distance($a, $b);
		if ($dist == 0){
			open A, ">/tmp/$i";
			print A $a;
			open A, ">/tmp/$j";
			print A $b;
			print "$i\;$j\;$dist\;$dist1\n";
		}else{			
			print "$i\;$j\;$dist\;$dist1\n";
		}
	}
}


if(!$hdb->close()){
	my $ecode = $hdb->ecode();
	printf STDERR ("close error: %s\n", $hdb->errmsg($ecode));
}
