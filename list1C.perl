#!/usr/bin/perl
use strict;
use DB_File;
use Compress::LZF;

my $fname1 = "$ARGV[1]";
my %id2real;
open A, "$fname1";
while(<A>){
	chop();
	my ($id, $idreal) = split(/\;/, $_, -1);
	$id2real{$id}{$idreal}++;
}

my $b = new DB_File::HASHINFO;
$b ->{cachesize}=1000000000;
$b ->{nelem} = 100000;
$b ->{bsize} = 4096;

my $fname="$ARGV[0]";
my (%clones);
tie %clones, "DB_File", $fname, O_RDONLY, 0666, $b
	or die "cant open file  $fname\n";

my $offset = 0;
while (my ($codeC, $vs) = each %clones){
	my $code = decompress ($codeC);
	my $lC = length($code);
	next if $lC == 0; 
	for my $v (keys %{ $id2real{$vs} }){
		print "$offset\;$lC\;$v\n";
	}
	$offset++;
}
untie %clones;
