use strict;
use warnings;
use Compress::LZF;
use TokyoCabinet;

use line;
#########################
# create code to versions database
#########################

my $pr = $ARGV[0];
my $tmpnam = $pr;
$tmpnam =~ s/^\.*\///g;
$tmpnam =~ s/\//_/g;

my $fname="$pr.tch";
my %clones;
if(!tie(%clones, "TokyoCabinet::HDB", "$fname", TokyoCabinet::HDB::OWRITER | TokyoCabinet::HDB::OCREAT)){
   print STDERR "tie error\n";
}

my $idx = -1; open A, "$pr.idx.last"; while(<A>){ chop(); $idx = $_; };
open IDX, ">>$pr.idx";
open IDXMAX, ">>$pr.idx.last";
my $repo=$ARGV[1];
my %cmds;
while(<STDIN>){
	chop();
	my ($rev, $f, @x) = split(/\;/, $_, -1);
	my ($vs, $hash) = split(/:/, $rev, -1);
	my $cmd = "cd $repo\;hg cat -r$vs "."'".$f."'";	
	my $path = "$repo/$f";
	if ($path ne ""){
		next if !($path =~ /$matchExt/);
		my $line = "";
		if (open CONT, "$cmd |"){
			while(<CONT>){
				$line .= $_;
			}
			close(CONT);
		}else{
			print STDERR "noread\;$cmd\;$path\;$vs\n";
		}
		my $len = length($line);
		next if $len == 0;
		my $lineC = compress ($line);
		if (defined $clones{$lineC}){
			my $id = $clones{$lineC};
			print IDX "$id\;$len\;$path/$vs\n";
		}else{
			$idx++;
			$clones{$lineC} = $idx;
			print IDX "$idx\;$len\;$path/$vs\n";
			print IDXMAX "$idx\n";
		}
	}
}
untie %clones;
