use strict;
use warnings;
use Compress::LZF;

use line;
#########################
# create code to versions database
#########################
use DB_File;
my $b = new DB_File::HASHINFO;
$b ->{cachesize}=1000000000;
$b ->{nelem} = 100000;
$b ->{bsize} = 4096;

my $pr = $ARGV[0];
my $tmpnam = $pr;
$tmpnam =~ s/^\.*\///g;
$tmpnam =~ s/\//_/g;

my $fname="$pr.tch";
my %clones;
tie %clones, "DB_File", $fname, O_RDWR|O_CREAT, 0666, $b
	or die "cant open file  $fname\n";
my $idx = -1;
open A, "$pr.idx.last";
while (<A>){
	chop();
	$idx = $_;
}
open IDX, ">>$pr.idx";
open IDXMAX, ">>$pr.idx.last";
my $repo=$ARGV[1];
my %cmds;
while(<STDIN>){
	chop();
	my $cmd = $_;
	
	my $path = $cmd;
	$path =~ s/^bzr cat -r([0-9\.]+) \'//;
	my $vs = $1;
	$path =~ s/\'$//;
	if ($path ne ""){
		next if !($path =~ /$matchExt/);
		my $line = "";
		if (open CONT, "$cmd |"){
			while(<CONT>){
				$line .= $_;
			}		
		}else{
			print STDERR "noread\;$cmd\;$path\;$vs\n";
		}
		my $len = length($line);
		next if $len == 0;
		my $lineC = compress ($line);
		if (defined $clones{$lineC}){
			my $id = $clones{$lineC};
			print IDX "$id\;$len\;$path/$vs\n";
		}else{
			$idx++;
			$clones{$lineC} = $idx;
			print IDX "$idx\;$len\;$path/$vs\n";
			print IDXMAX "$idx\n";
		}
	}
}
untie %clones;
