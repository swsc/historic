use strict;
use warnings;
use Compress::LZF;

use line;
#########################
# create code to versions database
#########################
use TokyoCabinet;
sub enc {
        my $str = $_[0];
        $str =~ s/\[/\%5B/g;
        $str =~ s/\]/\%5D/g;
        $str =~ s/\#/\%23/g;
        $str;
}

my $fbase = $ARGV[0];
my $fname="$fbase.tch";
my %clones;
if(!tie(%clones, "TokyoCabinet::HDB", "$fname", TokyoCabinet::HDB::OWRITER | TokyoCabinet::HDB::OCREAT)){
      print STDERR "tie error for $fname\n";
}
my $idx = -1;
my %seen;
open IDX, "$fbase.idx";
while(<IDX>){
        chop();
        my ($i, $p) = split(/\;/, $_, -1);
        $idx = $i if $i > $idx;
        $seen{$p}++;
}
open IDX, ">>$fbase.idx";
open IDXL, ">>$fbase.idx.last";

my %cmds;
while(<STDIN>){
	chop();
	my $cmd = $_;
	my $path = $cmd;
	$path =~ s/^git \--git-dir\=\'//;
	$path =~ s/\'$//;
	$path =~ s/^([^\']*)\' show \'//;
	my $dir = $1;
	$dir =~ s/^git\///;
	$dir =~ s/\/\.git$//;
	$path =~ s/(.*):/$dir\//;
	my $vs = $1;
	next if defined $seen{"$path/$vs"};
	if ($path ne ""){
		next if $path =~ /\=\>/;#indicates copies/renames:ccbd73a450bd33362373a3a066beffb348f528a7:lib/Test/Simple/t/{ => Builder}/output.t
		next if !($path =~ /$matchExt/);
		my $line = "";
		if (open CONT, "$cmd |"){
			while(<CONT>){
				$line .= $_;
			}
		}
		my $len = length($line);
		next if $len == 0;
		my $lineC = compress ($line);
		if (defined $clones{$lineC}){
			my $id = $clones{$lineC};
			print IDX "$id\;$len\;$path/$vs\n";
		}else{
			$idx++;
			$clones{$lineC} = $idx;
			print IDX "$idx\;$len\;$path/$vs\n";
			print IDXL "$idx\n";
		}
	}
}
untie %clones;
