#!/bin/bash

i=$1
from=allrepos
base=Git
cloneDir=..

gunzip -c stripe1:/data/20140205/$from.$i.cpio.gz  | cpio -idum 
ls | while read a; do if [[ -d $a ]]; then j=$(echo $a|sed 's/github.com_//;s/\.git$//'); git --git-dir=$a/.git log --numstat -M -C --diff-filter=ACMR --full-history --pretty=tformat:"STARTOFTHECOMMIT%n%H;%T;%P;%an;%ae;%at;%cn;%ce;%ct;%s" | perl ~/bin/extrgit.perl | cut -d\; -f1,9 |sed 's/;/:/;s|^|git --git-dir='"'"$a/.git"'"" show '"'|;s/$/'"'"'/';fi; done | gzip > $cloneDir/${base}$i.cmd.gz
nn=$(gunzip -c $cloneDir/${base}$i.cmd.gz|wc -l)
nchunk=$(echo "$nn/10000000"|bc)
nchunk1=$(echo "$nchunk+1"|bc)
nleft=$(echo "$nn-$nchunk*10000000"|bc)

for j in $(perl -e 'for my $j (1..'$nchunk'){print "$j\n";}')
do n=$(echo "$j*10000000"|bc)
  gunzip -c $cloneDir/Git$i.cmd.gz | head -$n | tail -10000000 | \
   perl -I ~/bin/ ~/bin/gitCloneTDB.perl $cloneDir/${base}"$i.$j" 2> $cloneDir/${base}"$i.$j".err
done 
gunzip -c $cloneDir/${base}$i.cmd.gz | tail -$nleft | \
perl -I ~/bin/ ~/bin/gitCloneTDB.perl $cloneDir/${base}"$i.$nchunk1" 2> $cloneDir/${base}"$i.$nchunk1".err 
