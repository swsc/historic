# README #

## Data Discovery, Collection, Storage and Processing Infrastructure ##

![workflow.PNG](https://bitbucket.org/repo/xLkzRp/images/4046105742-workflow.PNG)

## Discovery ##
**Discovering forges**
1. First strategy is to discover sites with large  collections of VCSs.(SourceForge, GoogleCode, Savannah supports CVS, SVN, Git ...)


2. Discover large and well-known projects like Linux kernel,  Gnome, KDE and Mozilla. These projects also have mini-forges involving projects related to the main project.


3. Focus on widely used projects in popular distributions,like Debian, RedHat, OpenSuse and package repositories such as rpmforge, provide a list of popular packages.


4. The fourth source of forges we discovered by investigating
third generation version control systems such as Git.
They had forges associated with that particular version control
systems at, for example, repo.or.cz, github.com, gitorious.
org, and git.debian.org. The list of URLs pointing to
project-specific VCS could be constructed using project IDs
obtained from the index page.


5. We looked at published literature for a survey of projects
and found, in, for example, by Koch [10], other VCS repositories
including wireshark, mono, gnustep, python, plt,
samba, gnucash, rastasoft, lyx, ompi, scipy, svn, ACE, fpc,
and freeciv.


6. In addition to forges that host VCSs, there are directories
of open source projects that list home page URLs and
other information about the projects. RawMeat and FSF are
two prominent examples of such directories. they
do provide pointers to source code snapshots in tarballs and
project home pages.

**However most open-source projects have migrated(or have a fork in) to Github and Bitbucket**

### Discovering list of projects within forges ###

Open-source forges like Bitbucket and Github, usually provide rest API to discover the list of projects, while others don't(a spider may be needed).  
**Reference:**  

* Crowdsourcing the discovery of software repositories in an educational environment
* Amassing and indexing a large sample of version control systems: towards the
census of public source code history -- Audris

### Output from discovery ###

* A list of git/hg/svn.. urls
* We divide the list so that each sub-list would result in approximately 2TB of storage for the cloned data(see below)
* We select only urls that have been updated or have not been previously selected  

**E.g.(The number of projects in each sub-lists varies violently)**

```
wc -l /da3_data/git/list20151010.{0,100}
  2841118      list20151010.0
  142          list20151010.100

```

## Retrieval - Create Level 1 data##

* Cloning
    * The latest VCSs(Git, Mercurial, Bazaar) all have clone functionality to create a replica of the repository
    * All subversion repositories allow mirroring using svm package. 
    * Most projects now use git or have been mirrored to git!
* Cloning is done on newton cluster for one 2TB two chunks at a time (three/four days)
* The chunk is then archived using modified pax (tar and cpio have max file size restrictions)
* The pax files are currently stored offline on 4/8/10TB disks

## Extraction ##

### Extraction of metadata: Level 2 data ###

* All VCSs mentioned provide APIs to list and extract versions and associated metadata. 
    * E.g. ‘cvs log’ or analogous commands produces ....log.gz file 
    * The output is then converted via ~/bin/extrgitPlog.perl into a table format with each row including file, version, date, author, and comment in ...deltaall.gz on /da1_data/delta
* Metadata extraction is time consuming: takes several days per chunk
* It can be done on newton, beacon, or da cluster
    * The chunk is broken down into 100-500 sub-chunks and run via grid engine
    * The final .deltaall.gz file merges info from sub-chunks.  

**E.g. for git**
```
#Iterate over i in the chunk:
git --git-dir="$i" log --all  \
                       --numstat -M -C  \
                       --full-history \   
                       --pretty=tformat:"STARTOFTHECOMMIT:$i%n%H;%T;%P;%an;%ae;%at;%cn;%ce;%ct;%s%n%b%nNOTES%N"


```

### Extraction of file contents: Level 2 data ###

![100hashtables.PNG](https://bitbucket.org/repo/xLkzRp/images/3360727759-100hashtables.PNG)

**Algorithm**  

1. On da/newton/beacon: 
    * results of each git get stored are compressed and stored in Chunk.bin file
2. On da3 (with 12TB ssd): 
    * Chunk.bin is read and each value is
        * Prehashed to determine in which database [0-99] to store
        * TokyoCabinet used for 100 hash databases
            * File content is stored as index*.tch on /da3_data/bkp/All.new
        * Print log to avoid making changes to the database
            * In /da3_data/bkp/All.new.idx.*  

**Rationality**  

1. To parallelize the post processing
2. To maximize storage speed for the content of each version of each file

## Physical architecture ###

### Our servers’ configuration ###

* 4 servers (da0 – da3) running
* Hardware parameters of each server are listed as follows:
    * da0,da1,da2:
        * CPU  number: 24
        * CPU: Intel(R) Xeon(R) CPU E5-2630 v2 @ 2.60GHz
        * RAM: 377GB
        * Disk: 35TB
    * da3:
        * CPU  number: 8
        * CPU: Intel(R) Xeon(R) CPU E5-2623 v3 @ 3.00GHz
        * RAM: 377GB
        * 12TB SSD
        * Disk: 70TB
* Additional storage currently offline: 13 ×10TB + 30 ×8TB = 370TB
* New server: 740GB RAM+100TB storage+up to 22TB SSD

[Newton HPC Cluster](https://newton.utk.edu/doc/ProgramDescription) and [Beacon cluster](https://www.nics.tennessee.edu/computing-resources/beacon/configuration) are also available.

### working environment ###

#### Connect to our servers ####

We have sshd installed on our server. You can use ssh to connect to our servers after sending your public key and verified.

```
#ssh -da[0-3]
# ssh username@host -p [port]

yuxing@yuxing-Lenovo-I1000:~/.ssh$ ssh yma28@da3.eecs.utk.edu -p 443
Last login: Mon Oct 24 23:17:15 2016 from user-24-96-104-89.knology.net
[yma28@da3]~% 
```


#### Docker ####

Docker is a light weight analogue to virtual machine(slightly different). It provides you the executing context(environment) for your code and software which benefits you from getting rid of configuration and installing dependent packages. Besides, it offers a layer of encapsulation. While you are inside you docker image or container, you can make changes to your working environment(like installing new version of python) without the concern of ruining other people's customization.

How to use docker?  

1. Build a docker image in [4 steps](https://docs.docker.com/engine/tutorials/dockerimages/)
    * You can find your base docker image in [Docker hub](https://hub.docker.com/)
2. Run a docker image  

```
# docker images will list all your images
[yma28@da3]~% docker images
REPOSITORY                           TAG                 IMAGE ID            CREATED             SIZE
audris/jupyter-r                     latest              17623d01dd91        5 weeks ago         2.108 GB
audris/jupyter-r                     <none>              bccc7ad7ad67        7 weeks ago         2.201 GB
audris/ipython-pymongo16             latest              20382e7d11bd        8 weeks ago         5.578 GB
audris/conda                         latest              fc85819fce87        8 weeks ago         3.262 GB
...
# docker ps will list all your running images(container)
yuxing@yuxing-Lenovo-I1000:~/.ssh$ docker ps 
CONTAINER ID        IMAGE                         COMMAND             CREATED             STATUS              PORTS               NAMES
b34c46f40629        yuxing/ipython-pymongo:v1.2   "/bin/bash"         59 seconds ago      Up 58 seconds       8888/tcp            mad_mcclintoc
...
# docker ps -a will list all containers
yuxing@yuxing-Lenovo-I1000:~/.ssh$ docker ps -a
CONTAINER ID        IMAGE                         COMMAND                  CREATED              STATUS                       PORTS               NAMES
b34c46f40629        yuxing/ipython-pymongo:v1.2   "/bin/bash"              About a minute ago   Up About a minute            8888/tcp            mad_mcclintock
09cc88d57659        yuxing/ipython-pymongo:v1.2   "/bin/bash"              2 minutes ago        Exited (0) 2 minutes ago                         sharp_panini
a6e295f2adf0        yuxing/ipython-pymongo:v1.2   "/bin/bash"              5 months ago         Exited (0) 5 months ago                          evil_brown
...


#docker run -it -w [containerdirectory] -p [IP:host port]:[container port] -v [host directory]:[container directory] image-name first-command 
# -it running a interactive terminal(TTY) when running image
# -w set up the working directory of container
# -p bind a container port to a host port
# -v mount a host directory on a container directory
# image-name is the image you build and want to run
# first-command: the first command executed in container

yuxing@yuxing-Lenovo-I1000:/home$ docker run -it -w /home/yma_docker -p 127.0.0.1:8081:8089 -v /home/yuxing:/home/yma_docker yuxing/ipython-pymongo:v1.2 /bin/bash
root@5021d9195f18:/home/yma_docker# 
#now if you run ls, you will see that you are actually in your host home directory(/home/yuxing)

```

**Docker swarm**  
Docker swarm is the clustering of docker image/container over a group of computers. In case that one task is very large which can be split into parts and parallelized, we may want to deploy it on 4 da servers and keep it running simultaneously. Docker provides its own way of clustering -- docker swarm. It offers a docker manager to communicate with us and helps monitor the operating status of all docker containers in this cluster. We can deploy our task among this cluster via sending command to docker swarm manager, which does heal the pain of talking to all four servers.




### Case study: Emberjs ###
Scripts are related to Emberjs if they contain 'emberjs' in their names.  
* **col_Emberjs_commit.sh** collects all commits info of project Emberjs  
* **col_same_path_with_Emberjs_commit.sh** collects commits in which its path field value is included in Emberjs paths
* **group_on_emberjs_path.py** groups the commits based on path field value  
* **statistics_on_emberjs.py** calculates:  
1. For each file path in Emberjs, how many projects also contain it  
2. For each file path in Emberjs, how many authors do we have    
3. Among those non-Emberjs projects we found, how many are clones(forks)