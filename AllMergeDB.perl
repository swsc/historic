use strict;
use warnings;
use Error qw(:try);
use DB_File;
use TokyoCabinet;
use splittc;
use Compress::LZF;

my $fbase="/store/clone/ALL";
my %clones;
tie %clones,  'splittc', "$fbase", TokyoCabinet::HDB::OWRITER |
	TokyoCabinet::HDB::OCREAT, 100
   or die "cant open $fbase\n";

my $idx = -1;
open IDX, ">>$fbase.idx";
open IDXL, ">>$fbase.idx.last";

open A, "$fbase.idx.last";
while(<A>){
	chop();
	$idx = $_;
}

while (<STDIN>){
	chop();
	$_ =~ s/\.(tch|db)$//;
	my $readFileBase = $_;
	my %id2n = ();	
	open IDXR, "$readFileBase.idx";
	my ($hasSize, $noSize) = (0, 0);
	while(<IDXR>){
		chop();
		my ($i, @p) = split(/\;/, $_, -1);
		if ($#p == 1){
			$id2n{$i}{"$p[0]\;$readFileBase/$p[1]"}++;
			$hasSize++;
		}else{
			if ($#p == 0){
				$id2n{$i}{"$readFileBase/$p[0]"}++;
				$noSize++;
			}else{
				if ($#p > 1){
					print STDERR "too many $readFileBase\;$_\n";
				}
			}
		}
	}
	if ($hasSize && $noSize){
		print STDERR "not Clear if it $readFileBase has size:$hasSize or no:$noSize\n";
		next;
	}

	my %cl;
	if(!tie %cl, "DB_File", "$readFileBase.db", O_RDONLY, 0666, $DB_File::DB_HASH){
		print STDERR "tie error for $readFileBase.db\n";
		next;
	}
	while (my ($k, $i) = each %cl){
		if (!defined $id2n{$i}){
			print STDERR "not defined id2n for $i in $readFileBase\n";
			next;
		}
		my @vs = sort (keys %{ $id2n{$i} });
		my $id = "";
		if (defined $clones{$k}){
			$id = $clones{$k};
		}else{
			$idx++;
			$clones{$k} = $idx;
			$id = $idx;
			print IDXL "$idx\n";
		}
		if ($noSize){
			my $code = safeDecomp ($k);
			my $l = length ($code);
			for my $v (@vs){
				print IDX "$id\;$l\;$v\n";
			}
		}else{
			for my $v (@vs){
				print IDX "$id\;$v\n";
			}
		}
	}
	untie %cl;
}
untie %clones;

sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}

