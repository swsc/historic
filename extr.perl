#  Author: Audris Mockus
#
#Extract each revision from cvs log output
#

use Time::Local;

my $delta=1;
sub output {
	if ($reviewed eq ""){$reviewed=0;}
	if ($submitted eq ""){$submitted=0;}
	$log=~s/\:\:$//;
	$rev =~s/\;/SEMICOLON/g;
	$log =~s/\;/SEMICOLON/g;
	print "$ARGV[0]$delta:$.\;$f\;$mod\;$rev\;$vv\;$y/$m/$d $hr:$minu:$sec\;$stmp\;$a\;$nadd\;$ndel\;$s\;$log\n";
	$log="";
	$delta++;
}	

{
	$getheader=1;$getnames=0;$doRev=0;$vv="";%vers="";
	while(<STDIN>){
		chop ();
		#catch end of last revision information
		if (/^=============================================================================$/){
			#print STDERR "File ended\n";
			$getheader=1;$getnames=0;$doRev=0;$vv="";%vers="";
			&output ();
			next;
		}
		#catch the end of header and end of revision information
		if (/^----------------------------$/){
			#print STDERR "REVISION ended\n";
			if ($getheader){
				$getnames=0;			
				$getheader=0;			
				$doRev=1;
				next;
			}else{
				&output ();
				next;
			}
		}

		#process file header
		if($getheader){
			if (/Working file: (.+)$/){
				$f=$1;
				next;
			}
			if (/^RCS file: (.+)$/){
				$mod=$1;
				$mod=~s/\,v$//;		
				#$mod=~s/^\.\///;		
				#$mod=~s/\/[^\/]+$//;
				next;
			}
			if (/^keyword substitution:/){
				#ignore
				$getnames=0;
				next;
			}
			if (/^total revisions:/){
				#ignore
				next;
			}
			if (/^description:/){
				#ignore
				next;
			}
			if ($getnames){
				if(/^\s+[A-z]/){ 
					s/[ \t\n]//g;
					@x=split(/\:/);
					$vers{$x[1]}=$x[0];
					#print STDERR "$x[1]\;$x[0]\n";
					next;
				}
				#print "$x[1]\;$vers{$x[1]}\n";
			}
			if (/^symbolic names:/){
				#ignore
				$getnames=1;
				next;
			}
		}


		#process revision
		if($doRev){			
			if (/^revision ([0-9\.]+)$/){
				$rev=$1;
				undef $submitted;undef $reviewed;
				undef $y;undef $m;undef $d;undef $stmp;undef $vv;
				undef $a; undef $nadd;undef $ndel;
				undef $s;
				next;
			}
			if (/^date: (\d\d\d\d)[\-\/](\d\d)[\-\/](\d\d) (\d\d)\:(\d\d)\:(\d\d)( -\d\d\d\d)?;\s+author: (.+)\;\s+state: (.+)\;(.*)$/){
				$y=$1;
				$m=$2;
				$d=$3;
				$hr=$4; $minu=$5; $sec=$6;
					
				$stmp = &timelocal (($sec,$minu,$hr,$d,$m-1,$y-1900));
				
				$a=$8;
				$s=$9;
				$lin=$10;
				$nadd=0;$ndel=0;
				if($lin=~/\s+lines: \+(\d+) \-(\d+)$/){	
					$nadd=$1;
					$ndel=$2;
				}
				if($vers{$rev} ne ""){$vv=$vers{$rev};}
				next;
			}
			if (/Submitted by:\s+(.+)$/){
				$submitted=$1;
				next;
			}
			if (/Reviewed by:\s+(.+)$/){
				$reviewed=$1;
				next;
			}
			#otherwise just a description
			$log =~ s/\;/SEMICOLON/g;
			$log .= "$_"."NEWLINE";
		}
	}
}









