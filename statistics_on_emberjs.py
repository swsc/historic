import gzip
file1 = '/da3_data/delta/emberjs_groups.list.gz'
file2 ='/da3_data/delta/groups_statistics_result'



#considering the first time and last one

with open(file2,'a+') as f2:
    with gzip.open(file1,'r') as f1:
        projects_store = []
        authors_store = []
        email_store = []
        fork_of_emberjs = 0
        for line in f1:
            g = line.strip()
            if g.endswith(':'):
                if g == ':':
                    f2.write(':' +'\n')
                else:
                    f2.write('Projects_num: '+ str(len(projects_store)) +';')
                    f2.write('Authors_num: '+ str(len(authors_store)) +';')
                    f2.write('Emails_num: '+ str(len(email_store)) +';')
                    f2.write('Forks_num: ' + str(fork_of_emberjs) + '\n')
                    groupname = g[:-1]
                    f2.write(groupname +':' +'\n')
                    projects_store = []
                    authors_store = []
                    email_store = []
                    fork_of_emberjs = 0
                continue
            elements = g.split(',')
            if elements[1] not in authors_store:
                authors_store.append(elements[1])
            if elements[2] not in email_store:
                email_store.append(elements[2])
            if elements[3] not in projects_store:
                projects_store.append(elements[3])
#calculation...now
            if '_ember.js' in elements[3]:
                fork_of_emberjs += 1
        f2.write('Projects_num: '+ str(len(projects_store)) +';')
        f2.write('Authors_num: '+ str(len(authors_store)) +';')
        f2.write('Emails_num: '+ str(len(email_store)) +';')
        f2.write('Forks_num: ' + str(fork_of_emberjs) + '\n')
        

