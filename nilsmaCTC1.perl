use strict;
use warnings;
use Compress::LZF;
use TokyoCabinet;
use Sclc;
use Error qw(:try);
use line;
use ccfinder;


my $fbase="$ARGV[0]";

my %id2f;
open A, "$ARGV[1]";
while(<A>){
        chop();
        my ($id, @f) = split(/\;/, $_, -1);
        my $fi = $f[$#f];
        my @x = split(/\//, $fi, -1);
        $fi = $x[$#x-1];
        $id2f{$id} = $fi;
}

sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}
sub safeLC {
	my ($fname, $code) = @_;
	try {
		my %y = Sclc::SourceCount($fname, $code);
		my $ncsl = $y{NCSL};
		return $ncsl;
	} catch Error with {
		my $ex = shift;
		print STDERR "Error: $ex\n";
		return "";
	}
}


my $fname=$fbase.".tch";
my (%clones, %ccfinder);
if(!tie(%clones, "TokyoCabinet::HDB", $fname,
	TokyoCabinet::HDB::OREADER)){
	print STDERR "tie error for $fname\n";
}

while (my ($codeC, $v) = each %clones){
	my $code = safeDecomp($codeC);
	my $lC = length($code);
	if ($lC > 0) {
		my $nl = scalar(split(/\n/, $code, -1));
		print STDERR "$v\;$lC\;$nl\;$v\;$id2f{$v}\n";
		my $nils = nilsimsa ($code);
		my $ncsl = 0;
		if ($id2f{$v} =~ /\.txt$/){
			$ncsl = $nl;
		}else{
			$ncsl = safeLC ($id2f{$v}, $code);
		}
		print "$v\;$lC\;$nl\;$ncsl\;$nils\n";
	}
}
untie %clones;

