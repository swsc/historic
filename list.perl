#!/usr/bin/perl
use strict;
use DB_File;

my $b = new DB_File::HASHINFO;
$b ->{cachesize}=1000000000;
$b ->{nelem} = 100000;
$b ->{bsize} = 4096;

my $fname="$ARGV[0]";
my (%clones);
tie %clones, "DB_File", $fname, O_RDONLY, 0666, $b
	or die "cant open file  $fname\n";

my $offset = 0;
while (my ($code, $vs) = each %clones){
	my $lC = length($code);
	#need to get date
	#need to get project name
	$vs =~ s/^\;//;
	for my $v (split(/\;/, $vs, -1)){
		print "$offset\;$lC\;$v\n";
	}
	$offset++;
}
untie %clones;
