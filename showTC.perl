use TokyoCabinet;
use Compress::LZF;
use strict;
use warnings;

sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}

my $fbase = $ARGV[0];
my %icl;
if (! tie (%icl,  'TokyoCabinet::HDB', "$fbase.itch", TokyoCabinet::HDB::OREADER, 16777213, -1, -1, TokyoCabinet::TDB::TLARGE, 100000)){
	die "cant open $fbase.itch\n";
}else{
}

my $codeC = $icl{$ARGV[1]};
my $code = safeDecomp ($codeC);

print "$code";

untie %icl;

