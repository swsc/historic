use strict;
use Digest::SHA1 qw (sha1_hex);
use TokyoCabinet;
use Compress::LZF;
use Error qw(:try);

my $fname1 = "$ARGV[1]";
my %id2real;
open A, "$fname1";
while(<A>){
	chop();
	my ($id, $idreal) = split(/\;/, $_, -1);
	$id2real{$id}{$idreal}++;
}
#my %real2v;
#open A, "$ARGV[2]";
#while(<A>){
#	chop();
#	my ($id, @rest) = split(/\;/, $_, -1);
#	$real2v{$id}{$rest[$#rest]}++;
#}

my $fname="$ARGV[0]";
my (%clones);
if(!tie(%clones, "TokyoCabinet::HDB", "$fname",
		  TokyoCabinet::HDB::OREADER)){
	print STDERR "tie error for $fname\n";
}

sub safeDecomp {
	my ($codeC, $vs) = @_;
	try {
		my $code = decompress ($codeC);
		return $code;
	} catch Error with {
		my $ex = shift;
		my @vv = keys %{ $id2real{$vs} };
		print STDERR "Error: $ex:$vs:@vv\n";
		return "";
	}
}

my $offset = 0;
while (my ($codeC, $vs) = each %clones){
	my $code = safeDecomp ($codeC, $vs);
	my $lC = length($code);
	my $shc = sha1_hex($code);
	my $shcC = sha1_hex($codeC);
	next if $lC == 0; 
	for my $v (keys %{ $id2real{$vs} }){
		print "$offset\;$lC\;$shc\;$v\n";
	}
	$offset++;
}
untie %clones;

1;
