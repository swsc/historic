use strict;
use warnings;
use Compress::LZF;
use TokyoCabinet;
use Error qw(:try);
use line;
use ccfinder;


my $fbase="$ARGV[0]";
my @x = split(/\//, $fbase, -1);
my $fname = pop @x;
my $fpath = join '/', @x;

my (%exts, %clones, %ccfinder);

my %id2f;
open A, "$ARGV[1]";
while(<A>){
	chop();
	my ($id, @f) = split(/\;/, $_, -1);
	$id2f{$id}{$f[$#f]}++;
}

if(!tie(%clones, "TokyoCabinet::HDB", "$fbase",
	TokyoCabinet::HDB::OREADER)){
	print STDERR "tie error for $fbase\n";
}

if(!tie(%ccfinder, "TokyoCabinet::HDB", "$ARGV[2].tch",
  TokyoCabinet::HDB::OWRITER | TokyoCabinet::HDB::OCREAT)){
  print STDERR "tie error for $ARGV[2].tch\n";
}
open IDX, "> $ARGV[2].idx";
open IDXL, "> $ARGV[2].idx.last";

our $matchRealC = q/\.(C|CPP|Cpp|H|S|ada|asm|c|c\+\+|cc|cce|cp|cpp|cpps|cs|csp|cxx|def|ec|erl|f|for|h|hh|hhk|hpp|hrc|hs|hxx|j|jav|java|java,|lisp|m|ml|mli|pas|ph|s|ss|st|t)$/;
my $idx = -1;
while (my ($codeC, $v) = each %clones){
	my $code = safeDecomp ($codeC);
	my $lC = length($code);
	if ($lC > 0) {
		my $ccf = safeCC ($code);
		if (length($ccf)>0){
			my $ccfC = compress $ccf;
			my $id = -1;
			if (defined $ccfinder{$ccfC}){
				$id = $ccfinder{$ccfC};
				#print IDX "$id\;$v\n";
			}else{
				$idx++;
				$ccfinder{$ccfC} = $idx;
				$id = $idx;
				print IDXL "$id\n";
			}
			for my $ext (keys %{ $id2f{$v} }){
				print IDX "$id\;$v\;$ext\n";
			}
		}
	}
}
untie %ccfinder;
untie %clones;

sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}

sub safeCC {
	my $code = $_[0];
   try {
		my $ccf = ccfinder ($code);
      return $ccf;
   } catch Error with {
		my $ex = shift;
      print STDERR "Error: $ex\n";
      return "";
	}
}
