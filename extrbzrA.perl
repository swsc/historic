use strict;
#  Author: Audris Mockus
#
#Extract each revision from bzr log -v --long output
#

require "timelocal.pl";

my %paths = ();
my ($rev, $bname, $cname,  $ctime, $comment) = ("","","","","","","","","","");
my ($getHeader, $getMessage, $getPaths) = (0, 0, 0);

sub output {
	foreach my $f (keys %paths){
		$comment =~ s/\r/ /g;
		$comment =~ s/\;/SEMICOLON/g;
		$comment =~ s/\n/__NEWLINE__/g;
		print "$rev\;$f\;$bname\;$cname\;$ctime\;$comment\n";
	}
	%paths = ();
	($rev, $bname, $cname, $ctime, $comment) = ("","","","","","","","","","");
}


while(<STDIN>){
	chop ();	
	#catch end of last revision information
	if (/^\s*revno: ([\d.]*)$/){
		my $newrev=$1;
		&output ();
		$getHeader=1;$getPaths=0;
		$rev = $newrev;
		next;
	}
	if ($getHeader){
		$cname = $1 if (/^\s*committer: (.*)$/);
		$bname = $1 if (/^\s*branch nick: (.*)$/);
		$ctime = $1 if (/^\s*timestamp: (.*)$/);
		if (/^\s*message:$/){
			$getMessage = 1;
			$getHeader=0;
			next;
		}
		next;
	}
	if ($getMessage){
		if (/^\s*(modified|added):$/){
			$getMessage = 0;
			$getPaths = 1;
			next;
		}else{
			$comment .= "$_ NEWLINE";
		}
	}
	if ($getPaths){
		$_ =~ s/^\s*//;
		if (/^----------------------------------/){
			$getPaths = 0;
			next;
		}
		$paths{$_}++;
		next;
	}
}

&output ();
