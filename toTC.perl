#!/usr/bin/perl
#for i in $(perl -e 'for $i (0..49){print "$i\n";}')
#do echo $i
#  rm /tmp/index_${i}.tch
#  perl ~/bin/toTC.perl index_$i /tmp/index_$i 2> /tmp/index_${i}.cvt
#  perl ~/bin/verifyTC.perl /tmp/index_${i}.tch index_${i}.db 1 > /tmp/index_${i}.cmp 2> /tmp/index_${i}.err
#done


use strict;
use warnings;

use DB_File;
use Compress::LZF;

my $fname="$ARGV[0]";
my $fname1=$fname;
$fname1 = $ARGV[1] if defined $ARGV[1];

my (%db);
my $b = new DB_File::HASHINFO;
tie %db, "DB_File", "$fname.db", O_RDONLY, 0666, $b
	or die "cant open file  $fname.db\n";

use TokyoCabinet;
my %tch;
if(!tie(%tch, "TokyoCabinet::HDB", "$fname1.tch", TokyoCabinet::HDB::OWRITER|TokyoCabinet::HDB::OCREAT)){
      print STDERR "tie error for $fname1.tch\n";
}

my $off = 0;
while(my ($key, $value) = each(%db)){
	#my $code = decompress ($key);
	$tch{$key} = $value;
	my $l = length ($key);
	print STDERR "$off\;$value\;$l\n";
	$off ++;
}
untie(%tch);
untie(%db);

