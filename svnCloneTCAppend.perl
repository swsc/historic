use strict;
use warnings;
use line;
#########################
# create code to versions database
#########################
use Compress::LZF;
use TokyoCabinet;

sub enc {
	my $str = $_[0];
	$str =~ s/\[/\%5B/g;
	$str =~ s/\]/\%5D/g;
	$str =~ s/\#/\%23/g;
	$str;
}

my $fbase="/tmp/nclone.svn.$ENV{DT}";
$fbase = $ARGV[1] if defined $ARGV[1];
open FS, "gunzip < $ARGV[0]|";
 
my $fname=$fbase.".tch";
my %clones;
if(!tie(%clones, "TokyoCabinet::HDB", "$fname", TokyoCabinet::HDB::OWRITER | TokyoCabinet::HDB::OCREAT, 16777213, -1, -1, TokyoCabinet::TDB::TLARGE, 100000)){
      print STDERR "tie error for $fname\n";
}
my $idx = -1;
my %seen;
open IDX, "$fbase.idx";
while(<IDX>){
	chop();
	my ($i, $size, $p) = split(/\;/, $_, -1);
	$idx = $i if $i > $idx;
	$seen{$p}++;
}
open IDX,  ">>$fbase.idx";
open IDXL, ">>$fbase.idx.last";
open VS,   ">>$fbase.vs";
while(<FS>){
	chop();
	my $cmd = "$_";
	my $path = $_;
	$path =~ s|^.*file:///.*/forge\.20[0-9]*/||;
	$path =~ s/\"$//;
	#$path =~ /^([^\/]*)\//;
	my @x = split(/\@/, $path, -1);
	my $vs = pop @x;
	$path = join "\@", @x;

	#this works for SVN as the revisions are listed last to first, once the one extracted before is detected
	#probably time to bolt
	last if defined $seen{"$path/$vs"};
	
	if ($path ne ""){
		next if !($path =~ /$matchExt/);
		$cmd = enc($cmd);
		open B, "$cmd |";
		my $line = "";
		while(<B>){
			$line .= $_;
		}
		close(B);
		my $length = length ($line);
		if ($line ne ""){
			my $cline = compress ($line);
			if (defined $clones{$cline}){
				my $ind = $clones{$cline};
            print IDX "$ind\;$length\;$path/$vs\n";
			}else{
				$idx++;
				$clones{$cline} = $idx;
				print IDX "$idx\;$length\;$path/$vs\n";
				print IDXL "$idx\n";
			}
		}
		my $size = length($line);
		print VS "$path/$vs\n";		
	}
}
close FS;
close VS;
close IDX;
close IDXL;
untie %clones;
