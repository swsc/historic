use TokyoCabinet;
use strict;
use warnings;


my $hdb = TokyoCabinet::HDB->new();

if(!$hdb->tune(16777213, -1, -1, TokyoCabinet::TDB::TLARGE)){
	my $ecode = $hdb->ecode();
	printf STDERR ("close error: %s\n", $hdb->errmsg($ecode));
}

if (!$hdb->open("$ARGV[0]", $hdb->OWRITER | $hdb->OCREAT)){
	my $ecode = $hdb->ecode();
   printf STDERR ("open error: %s\n", $hdb->errmsg($ecode));
}

if(!$hdb->optimize(16777213, -1, -1, TokyoCabinet::TDB::TLARGE)){
	my $ecode = $hdb->ecode();
	printf STDERR ("close error: %s\n", $hdb->errmsg($ecode));
}
if(!$hdb->close()){
	my $ecode = $hdb->ecode();
	printf STDERR ("close error: %s\n", $hdb->errmsg($ecode));
}
