cat gz/deltaAll gz/deltaOnly | while read i
do (gunzip -c gz/$i | grep -F github.com_emberjs_ember.js.git | perl -ane 'chop();@x=split(/\;/,$_,-1);print "$x[4]\;$x[6]\;$x[9]\;$x[11]\n"')&
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.0.gz

ls WoC/l2/gitP.delta.gz WoC/rykerE/gitP.delta.gz | while read i
do ( gunzip -c $i | \
	grep -F github.com_emberjs_ember.js.git | perl -ane 'chop();@x=split(/\;/,$_,-1);print "$x[4]\;$x[6]\;$x[9]\;$x[11]\n"') &
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.1.gz

ls WoC/l1/git.delta.gz WoC/l4/git.delta.gz WoC/pae/git.delta.gz | while read i
do (gunzip -c $i | \
	grep -F github.com_emberjs_ember.js.git | perl -ane 'chop();@x=split(/\;/,$_,-1);print "$x[2]\;$x[4]\;$x[7];$x[9]\n"')&
done |  lsort 30G -u -t\; -k1 -k2 -k3 | gzip > emberjs.2.gz

#gunzip -c gz/gsvnNew.10.delta.gz | head -1 | perl -I ~/bin -ane 'chop();@x=split(/\;/,$_,-1);use myParseDate;print "$x[7]\;$x[1]\;$x[2]\;$x[2]\;".(myParseDate($x[4]))."\;$x[5]\;$x[0]\;'$i'\n"'

ls gz/gsvn*.delta.gz  | sed 's"^gz/""' | while read i
do (gunzip -c gz/$i | \
   grep -F github.com_emberjs_ember.js.git | perl -I ~/bin -ane 'use myParseDate;chop();@x=split(/\;/,$_,-1);print "$x[2]\;$x[2]\;".(myParseDate($x[4]))."\;$x[5]\n"')&
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.3.gz

ls WoC/*/svn.delta.gz | while read i
do ( gunzip -c $i | \
   grep -F github.com_emberjs_ember.js.git | perl -I ~/bin -ane 'use myParseDate;chop();@x=split(/\;/,$_,-1);print "$x[2]\;$x[2]\;".(myParseDate($x[4]))."\;$x[5]\n"' )&
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.4.gz

#ls gz/googleNew.*.delta.gz | sed 's"^gz/""' | while read i
ls gz/googleNew.*.delta.gz | grep -v googleNew.1.delta.gz| sed 's"^gz/""' | while read i
do ( gunzip -c gz/$i | \
   grep -F github.com_emberjs_ember.js.git | perl -I ~/bin -ane 'use myParseDate;chop();@x=split(/\;/,$_,-1);print "$x[1]\;$x[1]\;".(myParseDate($x[3]))."\;$x[5]\n"') &
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.5.gz

ls gz/ghgNew.delta.gz | sed 's"^gz/""' | while read i
do (gunzip -c gz/$i | \
   grep -F github.com_emberjs_ember.js.git | perl -I ~/bin -ane 'use myParseDate;chop();@x=split(/\;/,$_,-1);print "$x[5]\;$x[4]\;".(myParseDate($x[3]))."\;$x[2]\n"') &
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.6.gz

ls WoC/*/hg.delta.gz | while read i
do (gunzip -c $i | \
	grep -F github.com_emberjs_ember.js.git | perl -I ~/bin -ane 'use myParseDate;chop();@x=split(/\;/,$_,-1);print "$x[5]\;$x[4]\;".(myParseDate($x[3]))."\;$x[2]\n"' )&
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.6.gz

ls gz/hgBB.delta.gz | sed 's"^gz/""' | while read i
do ( gunzip -c gz/$i | \
  grep -F github.com_emberjs_ember.js.git | perl -I ~/bin -ane 'use myParseDate;chop();@x=split(/\;/,$_,-1);print "$x[7]\;$x[6]\;".(myParseDate($x[5]))."\;$x[2]\n"') &
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.7.gz


ls WoC/*/cvs.delta.gz | while read i
do ( gunzip -c $i | \
  grep -F github.com_emberjs_ember.js.git | perl -I ~/bin -ane 'use myParseDate;chop();@x=split(/\;/,$_,-1);print "$x[7]\;$x[7]\;$x[6]\;$x[2]\n"' ) &
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.8.gz

cat gz/deltaExtra | while read i
do ( gunzip -c gz/$i | \
	grep -F github.com_emberjs_ember.js.git | perl -ane 'chop();@x=split(/\;/,$_,-1);print "$x[4]\;$x[6]\;$x[9]\;$x[11]\n"' )&
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.9.gz

ls gz/hgBBnew.delta.gz | sed 's"^gz/""' | while read i
do ( gunzip -c gz/$i | \
	  grep -F github.com_emberjs_ember.js.git | perl -I ~/bin -ane 'use myParseDate;chop();@x=split(/\;/,$_,-1);print "$x[7]\;$x[6]\;".(myParseDate($x[5]))."\;$x[2]\n"')&
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.10.gz

ls gz/gitBBnew.?.deltaall.gz | sed 's"^gz/""' | while read i
do ( gunzip -c gz/$i | \
    grep -F github.com_emberjs_ember.js.git | perl -ane 'chop();@x=split(/\;/,$_,-1);print "$x[4]\;$x[6]\;$x[9]\;$x[11]\n"') &
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.11.gz

cat left[1-4] | while read i
do ( gunzip -c $i | \
  grep -F github.com_emberjs_ember.js.git | perl -ane 'chop();@x=split(/\;/,$_,-1);print "$x[4]\;$x[6]\;$x[9]\;$x[11]\n"' ) &
done |  lsort 10G -u -t\; -k1 -k2 -k3 | gzip > emberjs.12.gz


# a proper way to update delta.idx compressed files
#perl -e '$i=13000000000;$n=-1;$pn=-1;while(<STDIN>){$n=int($i/1000000000);if ($n>$pn){open A, "|gzip > delta1.idx.$n.gz"}; print A "$_";$pn=$n;$i++;}' < delta.idx

