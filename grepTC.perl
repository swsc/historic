#!/usr/bin/perl
use strict;
use warnings;
use Error qw(:try);

use TokyoCabinet;
use Compress::LZF;




my $fname="$ARGV[0]";

my %i2n;
my $f = $fname;
$f =~ s/\.tch$//;
open A, "$f.idx";
while(<A>){
	chop();
	my ($i, @x) = split(/\;/, $_, -1);
	$i2n{$i}{$x[$#x]}++;
}

my (%clones);
if(!tie(%clones, "TokyoCabinet::HDB", "$fname",
		  TokyoCabinet::HDB::OREADER)){
	print STDERR "tie error for $fname\n";
}

sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}



my $offset = 0;
while (my ($codeC, $vs) = each %clones){
	my $code = safeDecomp ($codeC);
	my $n = 0;
	while ($code =~ /\bgoto\b/g){
		$n++;
	}
	if ($n>0){
		for my $f (keys %{ $i2n{$vs} }){
			print "$vs\;$n\;$f\n";
		}
	}
}
untie %clones;
