use TokyoCabinet;
use IO::Socket;
use Compress::LZF;
use strict;
use warnings;



sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}


my $fbase = $ARGV[0];
my %icl;
if (!tie (%icl,  'TokyoCabinet::HDB', "$fbase.itch", TokyoCabinet::HDB::OREADER, 16777213, -1, -1, TokyoCabinet::TDB::TLARGE, 100000)){
	die "cant open $fbase.itch\n";
}else{
}


my $server = new IO::Socket::INET(Listen    => 5,
                                    LocalAddr => 'romeo',
                                    LocalPort => 9000,
                                    Proto   => "tcp",
														Reuse => 1)
	or die "can't setup server";

print "SERVER Waiting for client connection on port 9000\n";

while (my $client = $server->accept()) {
$client->autoflush(1);
    while (<$client>) {
		 my $input = $_;;
		 chop($input);
		 #print $client "$input\n";
		 print "$input\n";
		 #while (my ($k, $v) = each %icl){
		 #	 print "$k\n";
		 #}
		 my $k = $input+0;
		 if (defined $icl{$k}){
		 	my $codeC = $icl{$k};
		 	my $code = safeDecomp ($codeC);
			print $client "$code";
			print $client "____FINISHED____\n"; 
		 }else{
			 print $client "NotFound\;".\%icl."\n";
		 }
    }
    close $client; 
}
untie %icl;		 


