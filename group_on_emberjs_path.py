from collections import defaultdict
import json
import gzip
file1 = '/da3_data/delta/emberjsFiles.gz'
file2 = '/da3_data/delta/emberjs_sort_clean.gz'
file3 = '/da3_data/delta/emberjs_groups'

All_paths = defaultdict(list)

with gzip.open(file1,'r') as emberpaths:
    for line in emberpaths:
        elements = line.strip().split(';')
        if not elements[2].isdigit():
            continue
        All_paths[str(elements[3])].append([str(elements[2]),str(elements[0]),str(elements[1]),'emberjs.git'])

with gzip.open(file2,'r') as matchedpaths:
    for line in matchedpaths:
        elements = line.strip().split(';')
        if len(elements) != 5:
            continue
        if not elements[2].isdigit():
            continue
        if str(elements[3]) not in All_paths:
            continue
        All_paths[str(elements[3])].append([str(elements[2]),str(elements[0]),str(elements[1]),str(elements[4])])

with gzip.open(file3+'.list.gz','a+') as groups1:
    for path in All_paths.keys():
        All_paths[path].sort(key = lambda x: x[0])
        groups1.write(str(path) + ':' + '\n')
        for item in All_paths[path]:
            groups1.write(','.join(item) + '\n')


