#!/usr/bin/perl
use strict;
use warnings;
use Error qw(:try);

use Digest::MD5 qw(md5 md5_hex md5_base64);
use TokyoCabinet;
use Compress::LZF;

my $fname="$ARGV[0]";
my (%clones);
if(!tie(%clones, "TokyoCabinet::HDB", "$fname",
		  TokyoCabinet::HDB::OREADER)){
	print STDERR "tie error for $fname\n";
}

sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}


my $offset = 0;
while (my ($codeC, $vs) = each %clones){
	#if ($vs eq "77381084"){
	#	open A, ">/tmp/77381084";
	#	print A $codeC;
	#	close A;
	#}
	my $code = safeDecomp ($codeC);
	my $lC = length($codeC);
	my $l = length($code);
	my ($dHC, $d64C, $dH, $d64) = (md5_hex($codeC), md5_base64($codeC), md5_hex($code), md5_base64($code));
	my $val = "$dHC\;$d64C\;$dH\;$d64";
	print "$offset\;$l\;$vs\;$lC\;$val\n";
	$offset++;
}
untie %clones;
#diff index_13.tch.list index_13.tch.list1
#224304c224304
#< 224303;16030;77381084;5315
#---
#> 224303;16030;76923807;5315
