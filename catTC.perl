use strict;
use warnings;
use Error qw(:try);

use TokyoCabinet;


my $PRJ = $ARGV[0];
my $fbase= $PRJ;
my %clones;
if (! tie (%clones,  'TokyoCabinet::HDB', "$fbase.tch", TokyoCabinet::HDB::OWRITER | TokyoCabinet::HDB::OCREAT)){
	die "cant open $fbase.tch\n";
}
my $idx = -1;
open A, "$fbase.idx.last";
while(<A>){
	chop();
	$idx = $_;
}
open IDX, ">>$fbase.idx";
open IDXL, ">>$fbase.idx.last";

while (<STDIN>){
	chop();
	$_ =~ s/\.tch$//;
	my $readFileBase = $_;

	my %id2n = ();	
	open IDXR, "$readFileBase.idx";
	while(<IDXR>){
		chop();
		my ($i, @p) = split(/\;/, $_, -1);
	  	$id2n{$i}{(join ';', @p)}++;		
	}
	my %cl;
	if(!tie(%cl, "TokyoCabinet::HDB", "$readFileBase.tch",
		  TokyoCabinet::HDB::OREADER)){
		print STDERR "tie error for $readFileBase.tch\n";
		next;
	}
	my %i2id = ();
	while (my ($k, $i) = each %cl){
		if (!defined $id2n{$i}){
			print STDERR "not defined id2n for $i in $readFileBase\n";
			next;
		}
		my @vs = sort (keys %{ $id2n{$i} });
		my $id = "";
		if (defined $clones{$k}){
			$id = $clones{$k};
		}else{
			$idx++;	
			$clones{$k} = $idx;
			$id = $idx;
			print IDXL "$idx\n";
		}
		$i2id{$i} = $id;
		for my $v (@vs){
			print IDX "$id\;$v\n";
		}
		
	}
	untie %cl;
}
untie %clones;
