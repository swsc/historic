use strict;
use warnings;
use Error qw(:try);

use TokyoCabinet;
use splittc;
use Compress::LZF;

my $fbase="/data/All.new";
my %clones;
tie %clones,  'splittc', "$fbase", TokyoCabinet::HDB::OWRITER |
	TokyoCabinet::HDB::OCREAT,
	16777213, -1, -1, TokyoCabinet::TDB::TLARGE, 100000,
	100
   or die "cant open $fbase\n";

my $idx = -1;

open A, "$fbase.idx.last";
while(<A>){
	chop();
	$idx = $_;
}

open IDX, ">>$fbase.idx";
open IDXL, ">>$fbase.idx.last";

while (<STDIN>){
	chop();
	$_ =~ s/\.bin$//;
	my $readFileBase = $_;
	my %id2n = ();	
	open IDXR, "$readFileBase.idx";
	my $base = $readFileBase;
	$base =~ s/^\/data\/tch\///;
	$base =~ s/^tch\///;
	$base =~ s/^\/store\/sources\///;
	$base =~ s/^\/store1\/sources\///;
	$base =~ s/^\.\///;
	open my $fh, '<', "$readFileBase.bin" or die ($!);
	while(<IDXR>){
		chop();
		my ($i, $siz, @p) = split(/\;/, $_, -1);
		my $k = "";
		my $rl = read($fh, $k, $siz);
		my $code = safeDecomp ($k);
		my $l = length ($code);
		my $id = "";
		if (defined $clones{$k}){
			$id = $clones{$k};
		}else{
			$idx++;
			$clones{$k} = $idx;
			$id = $idx;
			print IDXL "$idx\n";
		}
		print IDX "$id\;$l\;$base/$p[0]\n";
	}
}
untie %clones;

sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}

