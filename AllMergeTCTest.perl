use strict;
use warnings;
use Error qw(:try);

use TokyoCabinet;
use splittc;
use Compress::LZF;

my $fbase="/store1/clone/All.new";
my %clones;
my $idx = -1;
while (<STDIN>){
	chop();
	$_ =~ s/\.tch$//;
	my $readFileBase = $_;
	my %id2n = ();	
	open IDXR, "$readFileBase.idx";
	my $base = $readFileBase;
	$base =~ s/^\/store\/sources\///;
	$base =~ s/^\/store1\/sources\///;
	$base =~ s/^\.\///;
	my ($hasSize, $noSize) = (0, 0);
	while(<IDXR>){
		chop();
		my ($i, @p) = split(/\;/, $_, -1);
		if ($#p == 1 && $p[0] =~ /^[0-9]+$/){
			$id2n{$i}{"$p[0]\;$base/$p[1]"}++;
			$hasSize++;
		}else{
			my $pp = join ';', @p;
			if ($#p == 0){
				$id2n{$i}{"$base/$pp"}++;
				$noSize++;
			}else{
				if ($#p > 1 && $p[0] =~ /^[0-9]+$/){
					print STDERR "too many $readFileBase\;$_\n";
				}else{
					$id2n{$i}{"$base/$pp"}++;
					$noSize++;
					print STDERR "semicolon in the name? $base/$pp\n";
				}
			}
		}
	}
	if ($hasSize && $noSize){
		print STDERR "not Clear if it $readFileBase has size:$hasSize or no:$noSize\n";
		next;
	}

	my %cl;
	if(!tie(%cl, "TokyoCabinet::HDB", "$readFileBase.tch",
		  TokyoCabinet::HDB::OREADER)){
		print STDERR "tie error for $readFileBase.tch\n";
		next;
	}

	while (my ($k, $i) = each %cl){
		if (!defined $id2n{$i}){
			print STDERR "not defined id2n for $i in $readFileBase\n";
			next;
		}
	}
	untie %cl;
}

sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}

