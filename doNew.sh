#!/bin/bash
i=$1
cd /data/new/git
from=allreposNew
base=GitNew
cloneDir=..


#finish GitNew12.16

if [[ -f /stripe1/20140205/$from.$i.cpio.gz ]]
then
gunzip -c /stripe?/20140205/$from.$i.cpio.gz  | cpio -idum 
ls | while read a; do if [[ -d $a ]]; then j=$(echo $a|sed 's/github.com_//;s/\.git$//'); git --git-dir=$a log --numstat -M -C --diff-filter=ACMR --full-history --pretty=tformat:"STARTOFTHECOMMIT%n%H;%T;%P;%an;%ae;%at;%cn;%ce;%ct;%s" | perl ~/bin/extrgit.perl | cut -d\; -f1,9 |sed 's/;/:/;s|^|git --git-dir='"'"$a"'"" show '"'|;s/$/'"'"'/';fi; done | gzip > $cloneDir/${base}$i.cmd.gz
nn=$(gunzip -c $cloneDir/${base}$i.cmd.gz|wc -l)
nchunk=$(echo "$nn/10000000"|bc)
nchunk1=$(echo "$nchunk+1"|bc)
nleft=$(echo "$nn-$nchunk*10000000"|bc)

for j in $(perl -e 'for my $j (1..'$nchunk'){print "$j\n";}')
do n=$(echo "$j*10000000"|bc)
  gunzip -c $cloneDir/${base}$i.cmd.gz | head -$n | tail -10000000 | \
  perl -I ~/bin/ ~/bin/gitCloneTDB.perl $cloneDir/${base}"$i.$j" 2> $cloneDir/${base}"$i.$j".err
  scp -p $cloneDir/${base}"$i.$j".* lion:/store2/tch
  rm $cloneDir/${base}"$i.$j".*
done 
gunzip -c $cloneDir/${base}$i.cmd.gz | tail -$nleft | \
perl -I ~/bin/ ~/bin/gitCloneTDB.perl $cloneDir/${base}"$i.$nchunk1" 2> $cloneDir/${base}"$i.$nchunk1".err 
#  scp -p $cloneDir/${base}"$i.$j".* bear:/data/new
#  rm $cloneDir/${base}"$i.$j".*
fi
exit 0


i=11
fr=10
base=GitNew
cloneDir=..
rm $cloneDir/${base}"$i.$fr".*
nn=$(gunzip -c $cloneDir/${base}$i.cmd.gz|wc -l)
nchunk=$(echo "$nn/10000000"|bc)
for j in $(perl -e 'for my $j ('$fr'..'$nchunk'){print "$j\n";}')
do n=$(echo "$j*10000000"|bc)
  gunzip -c $cloneDir/${base}$i.cmd.gz | head -$n | tail -10000000 | \
  perl -I ~/bin/ ~/bin/gitCloneTDB.perl $cloneDir/${base}"$i.$j" 2> $cloneDir/${base}"$i.$j".err
  scp -p $cloneDir/${base}"$i.$j".* bear:/data/new
  rm $cloneDir/${base}"$i.$j".*
done  &


base=ggitNew
cloneDir=..
gunzip -c ../../20140205/ggitNew.gz | cpio -idum
ls | while read a; do if [[ -d $a ]]; then j=$(echo $a|sed 's/ggitNew_//;s/\.git$//'); git --git-dir=$a log --numstat -M -C --diff-filter=ACMR --full-history --pretty=tformat:"STARTOFTHECOMMIT%n%H;%T;%P;%an;%ae;%at;%cn;%ce;%ct;%s" | perl ~/bin/extrgit.perl | cut -d\; -f1,9 |sed 's/;/:/;s|^|git --git-dir='"'"$a"'"" show '"'|;s/$/'"'"'/';fi; done | gzip > $cloneDir/${base}.cmd.gz
gunzip -c $cloneDir/${base}.cmd.gz  | perl -I ~/bin/ ~/bin/gitCloneTDB.perl $cloneDir/${base} 2> $cloneDir/${base}.err