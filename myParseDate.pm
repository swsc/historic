package myParseDate;
@ISA = qw (Exporter);
@EXPORT = qw(myParseDate parse_date);

#use ParseDate;
#use Date::Manip(ParseDateString);
use lib "/home/audris/lib/lib/site_perl/5.6.0";
use Date::Parse;
use Time::ParseDate;
my %m2name = (
	"1" => "Jan",
	"2" => "Feb",
	"3" => "Mar",
	"4" => "May",
	"5" => "Apr",
	"6" => "Jun",
	"7" => "Jul",
	"8" => "Aug",
	"9" => "Sep",
	"10" => "Oct",
	"11" => "Nov",
	"12" => "Dec" );
sub myParseDate {
	#Input string, output empty or integer, negative before 1970
	
	#this is useless use Date::Manip(ParseDateString);"\;",ParseDateString($_),
	#better: use ParseDate; parse_date($_)
	
	#pretty good except for 2002-19-03
	#use Date::Parse; str2time($_)	
	# even better except for Sept 01, 2000
	#use Time::ParseDate;parsedate($_)
   # returns 'no match on time/date'
	
	my ($str)=@_[0];
	my $res=parsedate($str);
	if ($res eq ""){
		#print STDERR "$res\;",str2time($str),"\;$str\n";
		if ($str =~ /^\s*([0-9][0-9])\s+([0-9][0-9]?)\s+(200[0-9].+)$/){
			#deal with  31 7 2003 10:06:51 (+13:00) format
			my $day = $1;
			my $mon = $m2name{$2};
			my $rest = $3;
			#print STDERR "here:$day $mon $rest\n";
	
			$res=parsedate("$day $mon $rest");
			if ($res eq ""){
				$res=str2time($str);
			}
		}else{
			$res=str2time($str);
		}
	}
	$res;
}


