use strict;
use warnings;
use Error qw(:try);

use TokyoCabinet;


my $PRJ = $ARGV[0];
my $fbase= $PRJ;
my %clones;
if (! tie (%clones,  'TokyoCabinet::HDB', "$fbase.tch", TokyoCabinet::HDB::OWRITER | TokyoCabinet::HDB::OCREAT, 16777213, -1, -1, TokyoCabinet::TDB::TLARGE, 100000)){
	die "cant open $fbase.tch\n";
}else{
}

my %clonesC;
if (! tie (%clonesC,  'TokyoCabinet::HDB', "$fbase.ccf.tch", TokyoCabinet::HDB::OWRITER | TokyoCabinet::HDB::OCREAT, 16777213, -1, -1, TokyoCabinet::TDB::TLARGE, 100000)){
	die "cant open $fbase.ccf.tch\n";
}
my $idx = -1;
open A, "$fbase.idx.last";
while(<A>){
	chop();
	$idx = $_;
}
my $idxC = -1;
open A, "$fbase.ccf.idx.last";
while(<A>){
	chop();
	$idxC = $_;
}
my %seen;
open A, "$fbase.idx";
while(<A>){
	chop();
	my ($i, @p) = split (/\;/, $_, -1);
	
	$seen{i}{(join ';', @p)}++;
}
open A, "$fbase.ccf.idx";
while(<A>){
	chop();
	my ($i, @p) = split (/\;/, $_, -1);
	$seen{c}{(join ';', @p)}++;
}
open A, "$fbase.lines";
while(<A>){
	chop();
	my @x = split (/\;/, $_, -1);
	$seen{l}{$x[0]}++;
}
open A, "$fbase.oss";
while(<A>){
	chop();
	my ($i, @p) = split (/\;/, $_, -1);
	$seen{o}{(join ';', @p)}++;
}

print STDERR "Read all\n";

open IDX, ">>$fbase.idx";
open IDXL, ">>$fbase.idx.last";

open IDXC, ">>$fbase.ccf.idx";
open IDXCL, ">>$fbase.ccf.idx.last";

open LINES, ">>$fbase.lines";
open OSS, ">>$fbase.oss";

while (<STDIN>){
	chop();
	$_ =~ s/\.tch$//;
	print STDERR "doing $_\n";
	my $readFileBase = $_;

	my %id2n = ();	
	open IDXR, "$readFileBase.idx";
	while(<IDXR>){
		chop();
		my ($i, @p) = split(/\;/, $_, -1);
	  	$id2n{$i}{(join ';', @p)}++;		
	}
	my %id2oss = ();	
	open IDXR, "$readFileBase.oss";
	while(<IDXR>){
		chop();
		my ($i, @p) = split(/\;/, $_, -1);
		$id2oss{$i}{(join ';', @p)}++;
	}
	my %id2lines = ();	
	open IDXR, "$readFileBase.lines";
	while(<IDXR>){
		chop();
		my ($i, @p) = split(/\;/, $_, -1);
		$id2lines{$i} = join ';', @p;
	}
	
	my %cl;
	if(!tie(%cl, "TokyoCabinet::HDB", "$readFileBase.tch",
		  TokyoCabinet::HDB::OREADER)){
		print STDERR "tie error for $readFileBase.tch\n";
		next;
	}
	my %i2id = ();
	while (my ($k, $i) = each %cl){
		if (!defined $id2n{$i}){
			print STDERR "not defined id2n for $i in $readFileBase\n";
			next;
		}
		my @vs = sort (keys %{ $id2n{$i} });
		my $id = "";
		my %consider = ();
		for my $v (@vs){
			$consider{i}{$v}++ if (!defined $seen{i}{$v}); 
			$consider{o}{$v}++ if (!defined $seen{o}{$v}); 
		}
		if (exists ($clones{$k})){
			$id = $clones{$k};
			#do oss?
		}else{
			$idx++;	
			$clones{$k} = $idx;
			$id = $idx;
			print IDXL "$idx\n";
			for my $val (keys  %{ $id2oss{$i} }){
				print OSS "$idx\;$val\n";
			}
			print LINES "$idx\;$id2lines{$i}\n";
		}
		$i2id{$i} = $id;
		for my $v (keys %{ $consider{i} }){
			print IDX "$id\;$v\n";
		}
		
	}
	untie %cl;

	
	my %idc2id = ();	
	open IDXR, "$readFileBase.ccf.idx";
	while(<IDXR>){
		chop();
		my ($i, $id, @p) = split(/\;/, $_, -1);
		my $nid = $i2id{$id};
		my $pp = join ';', @p;
		$idc2id{$i}{"$nid\;$pp"}++;
	}		
	if(!tie(%cl, "TokyoCabinet::HDB", "$readFileBase.ccf.tch",
		  TokyoCabinet::HDB::OREADER)){
		print STDERR "tie error for $readFileBase.ccf.tch\n";
		next;
	}
	my $off = 0;
	while (my ($k, $i) = each %cl){
		if (!defined $idc2id{$i}){
			print STDERR "not defined idc2id for $i in $readFileBase.ccf\n";
			next;
		}
		#print STDERR "---\n$k\n" if $off == 274 || $off == 74574;
		my @vs = sort (keys %{ $idc2id{$i} });
		my %consider = ();
		for my $v (@vs){
			$consider{c}{$v}++ if (!defined $seen{c}{$v}); 
		}
		
		my $id = "";
		my $yes = 0;
		if (exists ($clonesC{$k})){
			$yes++;
			$id = $clonesC{$k};
		}else{
			$idxC++;
			#print STDERR "$idxC\;@vs\n" if $idxC== 159480 || $idxC == 85393;
			$clonesC{$k} = $idxC;
			$id = $idxC;
			print IDXCL "$idxC\n";
		}
		for my $v (keys %{ $consider{c} }){
			print IDXC "$id\;$v\n";
		}
		$off++;		
	}
	untie %cl;
}
untie %clones;
untie %clonesC;
