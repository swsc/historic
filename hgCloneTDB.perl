use strict;
use warnings;
use Compress::LZF;

use line;
#########################
# create code to versions database
#########################
sub enc {
        my $str = $_[0];
        $str =~ s/\[/\%5B/g;
        $str =~ s/\]/\%5D/g;
        $str =~ s/\#/\%23/g;
        $str;
}

my $fbase = $ARGV[0];
my $fname="$fbase.bin";
open F, ">$fname";

my $idx = 0;
open IDX, ">$fbase.idx";

my %cmds;
while(<STDIN>){
	chop();
	my ($repo, $rev, $f, @x) = split(/\;/, $_, -1);
	my ($vs, $hash) = split(/:/, $rev, -1);
	$f =~ s/'/'\\''/g;
	my $cmd = "cd $repo\;hg cat -r$vs "."'".$f."'";	
	#print "$cmd\n";
	my $path = "$repo/$f";
	if ($path ne ""){
		next if !($path =~ /$matchExt/);
		my $line = "";
		if (open CONT, "$cmd |"){
			while(<CONT>){
				$line .= $_;
			}
		}
		my $len = length($line);
		next if $len == 0;
		my $lineC = compress ($line);
		my $lenC = length($lineC);
		print IDX "$idx\;$lenC\;$path/$vs\n";
		print F $lineC;
		$idx+=$lenC;
	}
}
