use strict;
use line;
#########################
# create code to versions database
#########################
use Compress::LZF;
use TokyoCabinet;

my $fbase=$ARGV[0];
open FS, "find $fbase \-name '*,v'|";
my $fname=$fbase.".tch";
my %clones;
if(!tie(%clones, "TokyoCabinet::HDB", "$fname",
		  TokyoCabinet::HDB::OWRITER | TokyoCabinet::HDB::OCREAT)){
	print STDERR "tie error for $fname\n";
}

my $idx = -1;
open A, "$fbase.idx.last";
while(<A>){
        chop();
        $idx = $_;
}
print STDERR "starting from idx=$idx\n";

open VS, ">>$fbase.vs";
open IDX, ">>$fbase.idx";
open IDXL, ">>$fbase.idx.last";
while(<FS>){
	chop();
	my $path = $_;
	$path =~ s/,v$//;
	if ($path ne ""){
		next if !($path =~ /$matchExt/);
		my $id = $path;
		$id =~ s/^.\///;
		my $rcscmd = "rlog \"$path,v\" |perl -I$ENV{HOME}/bin $ENV{HOME}/bin/extr.perl|";
		open RV, $rcscmd or die "rlog $rcscmd\n";
		my %revs;
		while(<RV>){
			chop();
			my @x = split(/\;/, $_, -1);
			shift @x;
			shift @x;
			shift @x;
			my $rev = shift @x;
			my $rest = join ';', @x;
			$revs{$rev} = $rest;
		}
		close RV;
		foreach my $vsn (keys %revs){
			my $line = getRCS("$path,v", $vsn);
			if ($line ne ""){
				my $cline = compress ($line);
				if (defined $clones{$cline}){
					my $ind = $clones{$cline};
					print IDX "$ind\;$id/$vsn\n";
				}else{
					$idx++;
					$clones{$cline} = $idx;
					print IDX "$idx\;$id/$vsn\n";
					print IDXL "$idx\n";
				}
			}
			my $size = length($line);
			print VS "$path,v\;$vsn\;$revs{$vsn}\n";
		}
	}
	print STDERR "done $path\n";
}
close FS;
close VS;
untie %clones;

sub getRCS{
	my ($f, $rv) = @_;
	my $str= "";
	my $Rcmd = "co -p$rv \"$f\" |";
	open Z, $Rcmd or die "GetRCS $f-$rv\n";
	while(<Z>){
		$str .= $_;
	}
	close Z;
	$str;
}
