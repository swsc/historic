use strict;
use warnings;
use line;
#########################
# create code to versions database
#########################
use Compress::LZF;
use TokyoCabinet;

sub enc {
	my $str = $_[0];
	$str =~ s/\[/\%5B/g;
	$str =~ s/\]/\%5D/g;
	$str =~ s/\#/\%23/g;
	$str;
}

my $fbase="/tmp/nclone.svn.$ENV{DT}";
$fbase = $ARGV[1] if defined $ARGV[1];
open FS, "gunzip < $ARGV[0]|";

my $fname=$fbase.".tch";
my %clones;
if(!tie(%clones, "TokyoCabinet::HDB", "$fname", TokyoCabinet::HDB::OWRITER | TokyoCabinet::HDB::OCREAT)){
      print STDERR "tie error for $fname\n";
}
my $idx = -1;
my %seen;
open IDX, "$fbase.idx";
while(<IDX>){
	chop();
	my ($i, $p) = split(/\;/, $_, -1);
	$idx = $i if $i > $idx;
	$seen{$p}++;
}
open IDX, ">>$fbase.idx";
open IDXL, ">>$fbase.idx.last";
open VS, ">>$fbase.vs";
while(<FS>){
	chop();
	my $cmd = "$_";
	my $path = $_;
	$path =~ s/^.*http:\/\///;
	$path =~ s/^.*file:\/\/\/home\/(audris|store4)\/sources\/svnavaya\/forge\.[0-9]*\///;
	$path =~ s/^.*file:\/\/\/home\/(audris|store4)\/sources\/svnavaya\/forge\///;
	#$path =~ s/^.*file:\/\/\/home\/audris\/sources\/svnavaya\/$repo\.[0-9]*\//$repo\//;
	$path =~ s/^.*file:\/\/\/home\/(audris|store4)\/sources\/svnavaya\///;
	$path =~ s/^.*file:\/\/\/home\/store4\/sources\/svnavaya\///;
	$path =~ s/^.*svn:\/\/135.64.24.222\///;
	$path =~ s/^.*svn:\/\/iqbld364.dr.avaya.com\///;
	$path =~ s/^.*http:\/\/aes-svn.dr.avaya.com\/repos\///;
	$path =~ s/^.*http:\/\/plsvnap01.us1.avaya.com\/svn\///;
	$path =~ s/^.*https:\/\/apas-svn.du.rnd.avaya.com\/repo\///;
	$path =~ s/^.*http:\/\/etd-svn.hq.avaya.com\/repos\///;
	$path =~ s/^.*http:\/\/svn.avaya.com\/svn\///;
	$path =~ s/^.*https:\/\/svn.forge.avaya.com\/svnroot\///;
	$path =~ s/\"$//;
	$path =~ /^([^\/]*)\//;
	my $double = $1;
	my $rmdate = $double;
	$rmdate =~ s/\.20[01]\d\d\d\d\d$//;
	$path =~ s/^$double\/$rmdate\//$rmdate\//; #aes.20101020/aes for svm
	$path =~ s/^$double\//$rmdate\//;          #aes.20101020 for svn sync
	my @x = split(/\@/, $path, -1);
	my $vs = pop @x;
	$path = join "\@", @x;
	next if defined $seen{"$path/$vs"};
	if ($path ne ""){
		next if !($path =~ /$matchExt/);
		$cmd = enc($cmd);
		open B, "$cmd |";
		my $line = "";
		while(<B>){
			$line .= $_;
		}
		close(B);
		if ($line ne ""){
			my $cline = compress ($line);
			if (defined $clones{$cline}){
				my $ind = $clones{$cline};
            print IDX "$ind\;$path/$vs\n";
			}else{
				$idx++;
				$clones{$cline} = $idx;
				print IDX "$idx\;$path/$vs\n";
				print IDXL "$idx\n";
			}
		}
		my $size = length($line);
		print VS "$path/$vs\n";		
	}
}
close FS;
close VS;
close IDX;
close IDXL;
untie %clones;
